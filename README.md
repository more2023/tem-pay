# tem-pay

#### 介绍

tem-pay聚合支付，以实战、极简通用API能力接口、极少资源文件配置为主导的支付平台，封装了微信支付、支付宝支付，涵盖微信支付的JSAPI支付、Native支付、APP支付、H5支付、小程序支付、签约代扣、企业付款到零钱，支付宝支付的PCWeb支付、移动端web支付、APP支付，基于vert.x事件异步架构，异步编程模型，提供更优、更稳定的支付平台。

#### 技术说明

1.  基于vert.x轻量级、高性能框架，基于事件全异步编程模型，构建高性能、高可用平台。
2.  基于eventbus事件总线，使用通用的接口，不同的入参，少量的代码，能轻松路由到各支付网关，实现各种支付功能，方便拓展其他支付渠道的对接。
3.  基于redis分布式锁，实现接口幂等性解决方案。
4.  针对签名和报文，使用了更优的加密手段。签名域值采用“SHA1withRSA”进行签名，Json 报文数据主体密文采用“RSA/ECB/PKCS1Padding”进行加密。通过平台和接入方双秘钥的方式，确保支付数据安全性。
5.  系统预留了log4j2 + kafka的方式收集日志数据，写入eleasticsearch，方便后期搭建ELK统计分析，预警监控平台。
6.  基于RabbitMQ实现阶梯式异步回调通知，保障异步回调通知消息的一致性。
7.  基于guava本地缓存、redis、db，多级缓存模式，缓存热点数据，提供更优、更稳定的服务支撑。
8.  基于xxl-job分布式定时任务，保障分布式定时任务稳定性。


#### 安装教程

服务部署目录文件结构。

![输入图片说明](https://images.gitee.com/uploads/images/2020/0806/143238_5ceb13ec_676656.png "111.png")

依次进入各服务器，服务部署目录下，使用 ./start.sh start 启动服务； ./start.sh stop 停止服务。
服务日志目录： logs/record.log
nginx进行https代理，及多服务部署负载均衡。（也可以使用shell脚本，群起服务）。


![输入图片说明](https://images.gitee.com/uploads/images/2020/0806/143328_ad184c24_676656.png "222.png")

#### tem-pay-web后台管理系统

1.  tem-pay-web后台管理系统，主要是支撑tem-pay支付平台的数据统计、维护及运营和开展其他支付相关业务功能（如：对账、分布式定时任务、阶梯式异步回调通知等等）项目链接：https://gitee.com/more2023/tem-pay-web。
2.  tem-pay-web使用的技术栈，SpringBoot2.x、shiro、redis、RabbitMQ、xxl-job、thymeleaf、jquery、bootstrap。
3.  支持权限管理；redis管理session一致性，支持分布式部署；支持注解式多数据源事务(建议低并发系统使用)。
4.  界面风格展示：

![输入图片说明](https://images.gitee.com/uploads/images/2020/0806/145807_f82ebe90_676656.png "100.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0806/145822_eaa9f2ff_676656.png "101.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0806/145836_5ff7220d_676656.png "102.png")
#### 交流反馈

1.  有问题先看看 F&Q 中有没有相关的回答
2.  欢迎提交ISSUS，请写清楚问题的具体原因，重现步骤和环境(上下文)
3.  Java/微服务/支付/电商，交流请进群：1085211067
4.  个人博客：https://www.jianshu.com/u/c6a2bcc46f1d
