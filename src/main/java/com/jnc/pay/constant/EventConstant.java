package com.jnc.pay.constant;

/**
 * @Auther: jjn
 * @Date: 2020/6/24 11:08
 * @Desc:
 */
public class EventConstant {

    //支付交易事件Path
    public static final String EVENT_WX_PAY_NATIVE = "event.wechat.pay.native";
    public static final String EVENT_WX_PAY_JSAPI = "event.wechat.pay.jsapi";
    public static final String EVENT_WX_PAY_H5 = "event.wechat.pay.h5";
    public static final String EVENT_WX_PAY_APP = "event.wechat.pay.app";

    public static final String EVENT_ALI_PAY_PAGE = "event.ali.pay.pcweb";
    public static final String EVENT_ALI_PAY_WAP = "event.ali.pay.mobileweb";
    public static final String EVENT_ALI_PAY_ALIAPP = "event.ali.pay.app";


    //支付查询事件Path
    public static final String EVENT_WX_QUERY_NATIVE = "event.wechat.query.native";
    public static final String EVENT_WX_QUERY_JSAPI = "event.wechat.query.jsapi";
    public static final String EVENT_WX_QUERY_H5 = "event.wechat.query.h5";
    public static final String EVENT_WX_QUERY_APP = "event.wechat.query.app";

    public static final String EVENT_ALI_QUERY_PAGE = "event.ali.query.pcweb";
    public static final String EVENT_ALI_QUERY_WAP = "event.ali.query.mobileweb";
    public static final String EVENT_ALI_QUERY_ALIAPP = "event.ali.query.app";


    //退款交易事件Path
    public static final String EVENT_WX_REFUND_NATIVE = "event.wechat.refund.native";
    public static final String EVENT_WX_REFUND_JSAPI = "event.wechat.refund.jsapi";
    public static final String EVENT_WX_REFUND_H5 = "event.wechat.refund.h5";
    public static final String EVENT_WX_REFUND_APP = "event.wechat.refund.app";

    public static final String EVENT_ALI_REFUND_PAGE = "event.ali.refund.pcweb";
    public static final String EVENT_ALI_REFUND_WAP = "event.ali.refund.mobileweb";
    public static final String EVENT_ALI_REFUND_ALIAPP = "event.ali.refund.app";


    //退款查询事件Path
    public static final String EVENT_WX_REFUND_QUERY_NATIVE = "event.wechat.refund.query.native";
    public static final String EVENT_WX_REFUND_QUERY_JSAPI = "event.wechat.refund.query.jsapi";
    public static final String EVENT_WX_REFUND_QUERY_H5 = "event.wechat.refund.query.h5";
    public static final String EVENT_WX_REFUND_QUERY_APP = "event.wechat.refund.query.app";

    public static final String EVENT_ALI_REFUND_QUERY_PAGE = "event.ali.refund.query.pcweb";
    public static final String EVENT_ALI_REFUND_QUERY_WAP = "event.ali.refund.query.mobileweb";
    public static final String EVENT_ALI_REFUND_QUERY_ALIAPP = "event.ali.refund.query.app";


    //微信企业付款到零钱
    public static final String EVENT_WX_ENTER_PAY_CHANGE = "event.wechat.enter.pay.change";

    //微信公众号签约
    public static final String EVENT_WX_CONTRACT_JSAPI = "event.wechat.contract.jsapi";
    //微信APP签约
    public static final String EVENT_WX_CONTRACT_APP = "event.wechat.contract.app";
    //微信签约查询
    public static final String EVENT_WX_CONTRACT_QUERY = "event.wechat.contract.query";
    //微信解约
    public static final String EVENT_WX_TERMINATION = "event.wechat.termination";
    //微信代扣
    public static final String EVENT_WX_WITHHOLD = "event.wechat.withhold";


    //微信支付异步通知事件path
    public static final String EVENT_WX_PAY_CALLBACK_NOTIFY = "event.wechat.pay.callback.notify";
    //微信退款异步通知事件path
    public static final String EVENT_WX_REFUND_CALLBACK_NOTIFY = "event.wechat.refund.callback.notify";
    //微信签约、解约异步通知事件path
    public static final String EVENT_WX_CONTRACT_CALLBACK_NOTIFY = "event.wechat.contract.callback.notify";
    //支付宝支付、退款异步通知事件path
    public static final String EVENT_ALI_CALLBACK_NOTIFY = "event.ali.callback.notify";
    //微信代扣异步通知事件path
    public static final String EVENT_WX_WITHHOLD_CALLBACK_NOTIFY = "event.wechat.withhold.callback.notify";

    //统一异常处理事件path
    public static final String EVENT_EXCEPTION = "event.exception";
}
