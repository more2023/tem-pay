package com.jnc.pay.constant;

/**
 * @Auther: jjn
 * @Date: 2020/5/9 11:57
 * @Desc:
 */
public class RedisConstant {

    //字典值 hash key前缀；   key: dict:data:[dictType]    hashKey: dictCode    hashValue: dictValue
    public static final String DICT_DATA = "dict:data:";
    //微信支付和APP管理配置 key前缀；     key：wechatpay:config:[appId]     value：json
    public static final String WECHATPAY_CONFIG = "wechatpay:config:";
    //支付宝支付和APP管理配置 key前缀；     key：alipay:config:[appId]     value：json
    public static final String ALIPAY_CONFIG = "alipay:config:";

    //有效商户 hash key前缀；  key: valid:mch:[mchId]  hashKey: 字段    hashValue:  字段值
    public static final String VALID_MCH_LIST = "valid:mch:";
    //分布式锁key前缀；  key: lock:key:[lockKey]
    public static final String LOCK_KEY_PREFIX = "lock:key:";


    //IP白名单
    public static final String WHITE_LIST_IP = "white_list_ip";
    //支付类型跟event事件名称关联 dictType
    public static final String DICT_PAY_TYPE_EVENT = "pay_type_event";
    //查询类型跟event事件名称关联 dictType
    public static final String DICT_QUERY_TYPE_EVENT = "query_type_event";
    //退款类型跟event事件名称关联 dictType
    public static final String DICT_REFUND_TYPE_EVENT = "refund_type_event";
    //退款查询类型跟event事件名称关联 dictType
    public static final String DICT_REFUND_QUERY_TYPE_EVENT = "refund_query_type_event";
    //转账类型跟event事件名称关联 dictType
    public static final String DICT_TRANS_TYPE_EVENT = "trans_type_event";
    //签约类型跟event事件名称关联 dictType
    public static final String DICT_CONTRACT_TYPE_EVENT = "contract_type_event";
    //签约查询类型跟event事件名称关联 dictType
    public static final String DICT_CONTRACT_QUERY_TYPE_EVENT = "contract_query_type_event";
    //解约类型跟event事件名称关联 dictType
    public static final String DICT_TERMINATION_TYPE_EVENT = "termination_type_event";
    //代扣类型跟event事件名称关联 dictType
    public static final String DICT_WITHHOLD_TYPE_EVENT = "withhold_type_event";

}
