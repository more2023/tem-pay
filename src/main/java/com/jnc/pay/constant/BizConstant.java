package com.jnc.pay.constant;

/**
 * @Auther: jjn
 * @Date: 2020/5/7 19:54
 * @Desc:
 */
public class BizConstant {

    // 保存request对象   key
    public static final String REQ_KEY = "reqKey";
    // 保存Ip   key
    public static final String REQ_IP = "reqIp";

    //请求时间戳超时时间为5分钟 5 * 60 * 1000
    public static final int TIME_STAMP_OUT = 300000;

    //签名算法常量
    public static final String SIGN_TYPE_MD5 = "MD5";
    public static final String SIGN_TYPE_HMAC = "HMAC-SHA256";

    //支付渠道
    public static final String CHANNEL_ID_WX = "wechatpay";
    public static final String CHANNEL_ID_ALI = "alipay";
    public static final String CHANNEL_ID_UNION = "unionpay";

    //支付状态
    public static final Integer WAIT_PAY = 0;      //待支付
    public static final Integer PAY_SUCCESS = 1;   //支付成功
    public static final Integer PAY_BIZ_COMPLETE = 2;  //业务处理完成(通知成功之后)
    public static final Integer PAY_BIZ_CLOSED = 3;    //该交易已关闭(如订单过期)
    public static final Integer PAY_FAIL = -1;     //支付失败

    //退款状态
    public static final Integer WAIT_REFUND = 0;      //待退款
    public static final Integer REFUND_SUCCESS = 1;   //退款成功
    public static final Integer REFUND_BIZ_COMPLETE = 2;  //业务处理完成(通知成功之后)
    public static final Integer REFUND_BIZ_CLOSED = 3;    //该交易已关闭(如订单过期)
    public static final Integer REFUND_FAIL = -1;     //退款失败

    //转账状态
    public static final Integer WAIT_TRANS = 0;      //待转账
    public static final Integer TRANS_SUCCESS = 1;   //转账成功
    public static final Integer TRANS_BIZ_COMPLETE = 2;  //业务处理完成(通知成功之后)
    public static final Integer TRANS_BIZ_CLOSED = 3;    //该交易已关闭(如订单过期)
    public static final Integer TRANS_FAIL = -1;     //转账失败

    //签约、解约状态
    public static final Integer WAIT_CONTRACT = 0;      //待签约
    public static final Integer CONTRACT_SUCCESS = 1;   //签约成功
    public static final Integer CONTRACT_BIZ_COMPLETE = 2;  //签约业务处理完成(通知成功之后)
    public static final Integer CONTRACT_FAIL = 3;     //签约失败

    public static final Integer TERMINATION_SUCCESS = 5;   //解约成功
    public static final Integer TERMINATION_BIZ_COMPLETE = 6;  //解约业务处理完成(通知成功之后)
    public static final Integer TERMINATION_FAIL = 7;     //解约失败

    //退款类型
    public static final Integer REFUND_TYPE_BIZ = 0;  //业务退款
    public static final Integer REFUND_TYPE_REPEAT = 1;  //重复退款

    //退款方式
    public static final Integer REFUND_WAY_AUTO = 1;  //自动原路返回
    public static final Integer REFUND_WAY_MADE = 2;  //人工打款


    //通知类型
    public static final String NOTIFY_TYPE_PAY="pay";            //支付
    public static final String NOTIFY_TYPE_REFUND="refund";      //退款
    public static final String NOTIFY_TYPE_CANCELED="canceled";  //取消
    public static final String NOTIFY_TYPE_CONTRACT="contract";  //签约

    //通知状态
    public static final Integer NOTIFY_PENDING = 1;   //通知中
    public static final Integer NOTIFY_SUCCESS = 2;  //通知成功
    public static final Integer NOTIFY_FAIL = 3;    //通知失败

}
