package com.jnc.pay.constant;

/**
 * @Auther: jjn
 * @Date: 2020/7/7 16:35
 * @Desc:
 */
public class AliPayConstant {


    //支付宝支付网关（域名+端口）
    public static final String ALI_GATEWAY = "https://openapi.alipaydev.com/gateway.do";
    //支付宝支付参数类型
    public static final String PARAM_TYPE = "json";
    //支付宝支付版本
    public static final String API_VERSION = "1.0";
    //支付宝支付编码
    public static final String CHARSET_TYPE = "utf-8";

    //网关成功状态码
    public static final String CODE_SUCCESS = "10000";
    //业务成功状态码
    public static final String SUB_CODE_SUCCESS = "ACQ.TRADE_HAS_SUCCESS";

    //产品码
    public static final String PRODUCT_CODE_PAGE = "FAST_INSTANT_TRADE_PAY"; //pc web网页支付
    public static final String PRODUCT_CODE_WAP = "QUICK_WAP_WAY"; //mobile web网页支付
    public static final String PRODUCT_CODE_APP = "QUICK_MSECURITY_PAY"; //app支付

    //回调状态
    public static final String TRADE_STATUS_WAIT = "WAIT_BUYER_PAY";  //交易创建，等待买家付款
    public static final String TRADE_STATUS_SUCCESS = "TRADE_SUCCESS"; //交易支付成功
    public static final String TRADE_STATUS_CLOSE = "TRADE_CLOSED";  //未付款交易超时关闭，或支付完成后全额退款
    public static final String TRADE_STATUS_FINISHED = "TRADE_FINISHED";  //交易结束，不可退款

    //支付类型
    public static final String PAY_TYPE_PAGE = "PAGE";
    public static final String PAY_TYPE_WAP = "WAP";
    public static final String PAY_TYPE_APP = "ALIAPP";
}
