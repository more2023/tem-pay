package com.jnc.pay.constant;

/**
 * @Auther: jjn
 * @Date: 2019/12/6 17:10
 * @Desc:
 */
public class SysConstant {

    public static final String CHARSET_NAME = "UTF-8";   //字符集编码

    public static final String TX_BASE_NAME = "baseTransactionManager";  //基础事务管理器名称

}
