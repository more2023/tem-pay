package com.jnc.pay.constant;

/**
 * @Auther: jjn
 * @Date: 2020/7/6 17:34
 * @Desc:
 */
public class UriConstant {

    //微信支付回调接口URI
    public static final String WECHAT_PAY_CALLBACK_V1 = "/v1/notify/wpnh";
    //支付宝支付回调接口URI
    public static final String ALI_PAY_CALLBACK_V1 = "/v1/notify/apnh";

    //微信退款回调接口URI
    public static final String WECHAT_REFUND_CALLBACK_V1 = "/v1/notify/wrnh";
    //支付宝退款回调接口URI
    public static final String ALI_REFUND_CALLBACK_V1 = "/v1/notify/arnh";

    //微信签约回调接口URI
    public static final String WECHAT_CONTRACT_CALLBACK_V1 = "/v1/notify/cwnh";
    //微信代扣回调接口URI
    public static final String WECHAT_WITHHOLD_CALLBACK_V1 = "/v1/notify/whnh";
}
