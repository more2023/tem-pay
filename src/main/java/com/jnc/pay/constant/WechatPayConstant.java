package com.jnc.pay.constant;

/**
 * @Auther: jjn
 * @Date: 2020/6/28 16:10
 * @Desc: 微信支付常量
 */
public class WechatPayConstant {

    //签约版本号
    public static final String CONTRACT_VERSION = "1.0";

    public static final String SUCCESS_CODE = "SUCCESS";
    public static final String FAIL_CODE = "FAIL";

    public static final String PAY_TYPE_NATIVE = "NATIVE";
    public static final String PAY_TYPE_JSAPI = "JSAPI";
    public static final String PAY_TYPE_APP = "APP";
    public static final String PAY_TYPE_H5 = "MWEB";

    public static final String CONTRACT_TYPE_JSAPI = "JSAPI_CW";
    public static final String CONTRACT_TYPE_APP = "APP_CW";

    //微信支付网关（域名+端口）
    public static final String WECHAT_GATEWAY = "https://api.mch.weixin.qq.com";

    //支付URI
    public static final String WECHAT_PAY = "/pay/unifiedorder";
    //支付查询URI
    public static final String WECHAT_QUERY = "/pay/orderquery";
    //退款URI
    public static final String WECHAT_REFUND = "/secapi/pay/refund";
    //退款查询URI
    public static final String WECHAT_REFUND_QUERY = "/pay/refundquery";
    //企业付款到零钱
    public static final String WECHAT_PAY_CHANGE = "/mmpaymkttransfers/promotion/transfers";
    //公众号签约URI
    public static final String WECHAT_CONTRACT_JSAPI = "/papay/entrustweb";
    //APP签约URI
    public static final String WECHAT_CONTRACT_APP = "/papay/preentrustweb";
    //签约查询URI
    public static final String WECHAT_CONTRACT_QUERY = "/papay/querycontract";
    //解约URI
    public static final String WECHAT_TERMINATION = "/papay/deletecontract";
    //代扣URI
    public static final String WECHAT_WITHHOLD = "/pay/pappayapply";
}
