package com.jnc.pay.biz.common.vo;

import com.jnc.pay.core.model.BaseReq;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Auther: jjn
 * @Date: 2020/6/23 09:39
 * @Desc:
 */
@Setter
@Getter
public class PayReq extends BaseReq {

    /** 应用方订单ID */
    @NotBlank(message = "应用方订单ID不能为空")
    @Length(min = 1, max = 32, message = "订单ID长度不能超过32位")
    private String appOrderId;

    /** 支付金额 */
    @NotNull(message = "支付金额不能为空")
    @Min(value = 1, message = "支付金额最小不小于0.01元")
    @Max(value = 10000000, message = "支付金额最大不超过100000元")
    private Integer totalFee;
    /** 金额对应的小数位数(默认2位) */
    private Integer scale;
    /** 交易的币种(默认CNY) */
    private String currencyCode;
    /** 通知URL */
    @NotBlank(message = "通知URL不能为空")
    private String notifyUrl;
    /** 商品描述信息 */
    @NotBlank(message = "商品描述不能为空")
    private String body;
    /** 商品详细信息 */
    private String detail;

    /** =========================  各渠道各支付类型特定字段  ============================= */
    /** 用户标识(如openid，支付宝账号) */
    private String channelUserId;
    /** 支付后跳转url */
    private String returnUrl;
    /**  场景信息 */
    private String sceneInfo;
    /** 额外参数 */
    private String extra;
    /** 客户端IP */
    private String clientIp;

    /** ========================== 程序自定义字段 ============================ */

    //交易ID(程序生成)
    private Long tradeId;
    //平台回调url
    private String platformNotifyUrl;
    /** 签约id */
    private Long contractId;
    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("appOrderId", getAppOrderId())
                .append("appId", getAppId())
                .append("channelId", getChannelId())
                .append("payType", getPayType())
                .append("totalFee", getTotalFee())
                .append("scale", getScale())
                .append("currencyCode", getCurrencyCode())
                .append("channelUserId", getChannelUserId())
                .append("notifyUrl", getNotifyUrl())
                .append("extra", getExtra())
                .append("body", getBody())
                .append("detail", getDetail())
                .append("clientIp", getClientIp())
                .toString();
    }
}
