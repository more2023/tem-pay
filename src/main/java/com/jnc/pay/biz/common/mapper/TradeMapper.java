package com.jnc.pay.biz.common.mapper;

import com.jnc.pay.biz.common.model.Trade;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Auther: jjn
 * @Date: 2020/7/3 11:36
 * @Desc:
 */
@Mapper
public interface TradeMapper {
    /**
     * 新增支付订单
     * @param model
     * @return
     */
    public int addTrade(Trade model);

    /**
     * 根据ID，获取支付订单详情
     * @param tradeId
     * @return
     */
    public Trade getTradeById(Long tradeId);

    /**
     * 编辑支付订单
     * @param model
     * @return
     */
    public int editTrade(Trade model);

    /**
     * 根据appId和appOrderId，查询支付订单的数量
     * @param model
     * @return
     */
    public Integer queryCount(Trade model);
}
