package com.jnc.pay.biz.common.vo;

import com.jnc.pay.core.model.BaseReq;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Auther: jjn
 * @Date: 2020/8/3
 * @Desc: 微信企业付款到零钱
 */
@Setter
@Getter
public class PayChangeReq extends BaseReq {

    /** 应用方转账ID */
    @NotBlank(message = "应用方转账ID不能为空")
    @Length(min = 1, max = 32, message = "应用方转账ID长度不能超过32位")
    private String appTransId;

    /** 转账金额 */
    @NotNull(message = "转账金额不能为空")
    @Min(value = 1, message = "转账金额最小不小于0.01元")
    @Max(value = 10000000, message = "转账金额最大不超过100000元")
    private Integer totalFee;
    /** 金额对应的小数位数(默认2位) */
    private Integer scale;
    /** 交易的币种(默认CNY) */
    private String currencyCode;
    /** 通知URL */
    private String notifyUrl;
    /** 转账说明 */
    @NotBlank(message = "转账说明不能为空")
    private String transDesc;
    /** 用户标识(如openid，支付宝账号) */
    @NotBlank(message = "用户标识不能为空")
    private String channelUserId;



    /** 额外参数 */
    private String extra;
    /** 客户端IP */
    private String clientIp;

    /** ========================== 程序自定义字段 ============================ */
    //转账ID(程序生成)
    private Long transId;
}
