package com.jnc.pay.biz.common.mapper;

import com.jnc.pay.biz.common.model.Contract;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Auther: jjn
 * @Date: 2020/7/3 11:36
 * @Desc:
 */
@Mapper
public interface ContractMapper {
    /**
     * 新增签约订单
     * @param model
     * @return
     */
    public int addContract(Contract model);

    /**
     * 根据ID，获取签约订单详情
     * @param contractId
     * @return
     */
    public Contract getContractById(Long contractId);

    /**
     * 根据签约协议号，获取签约订单详情
     * @param contractCode
     * @return
     */
    public Contract getContractByCode(String contractCode);

    /**
     * 编辑签约订单
     * @param model
     * @return
     */
    public int editContract(Contract model);

    /**
     * 根据contractCode，查询签约订单的数量
     * @param model
     * @return
     */
    public Integer queryCount(Contract model);
}
