package com.jnc.pay.biz.common.work;

import com.jnc.pay.biz.common.vo.*;
import com.jnc.pay.biz.wechatpay.service.WechatPayAPP;
import com.jnc.pay.biz.wechatpay.service.WechatPayH5;
import com.jnc.pay.biz.wechatpay.service.WechatPayJSAPI;
import com.jnc.pay.biz.wechatpay.service.WechatPayNative;
import com.jnc.pay.constant.EventConstant;
import com.jnc.pay.core.model.BaseResp;
import com.jnc.pay.core.vertx.verticle.WorkerVerticle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @Auther: jjn
 * @Date: 2020/6/24 17:03
 * @Desc: 微信支付work
 */
@Slf4j
@Order(1)
@Component
public class WechatPayWork extends WorkerVerticle {

    @Autowired
    WechatPayNative wechatPayNative;
    @Autowired
    WechatPayJSAPI wechatPayJSAPI;
    @Autowired
    WechatPayH5 wechatPayH5;
    @Autowired
    WechatPayAPP wechatPayAPP;

    @Override
    public void start() throws Exception {

        //微信Native支付
        vertx.eventBus().consumer(EventConstant.EVENT_WX_PAY_NATIVE, event -> {
            PayReq req = (PayReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = wechatPayNative.doPay(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("WechatPayWork.pay.native exception: {}", e);
                exceptionHandler(e, event);
            }
        });
        //微信JSAPI支付
        vertx.eventBus().consumer(EventConstant.EVENT_WX_PAY_JSAPI, event -> {
            PayReq req = (PayReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = wechatPayJSAPI.doPay(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("WechatPayWork.pay.jsapi exception: {}", e);
                exceptionHandler(e, event);
            }

        });
        //微信H5支付
        vertx.eventBus().consumer(EventConstant.EVENT_WX_PAY_H5, event -> {
            PayReq req = (PayReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = wechatPayH5.doPay(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("WechatPayWork.pay.h5 exception: {}", e);
                exceptionHandler(e, event);
            }

        });
        //微信APP支付
        vertx.eventBus().consumer(EventConstant.EVENT_WX_PAY_APP, event -> {
            PayReq req = (PayReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = wechatPayAPP.doPay(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("WechatPayWork.pay.app exception: {}", e);
                exceptionHandler(e, event);
            }

        });


        //微信Native查询
        vertx.eventBus().consumer(EventConstant.EVENT_WX_QUERY_NATIVE, event -> {
            QueryReq req = (QueryReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = wechatPayNative.doQuery(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("WechatPayWork.query.native exception: {}", e);
                exceptionHandler(e, event);
            }

        });
        //微信JSAPI查询
        vertx.eventBus().consumer(EventConstant.EVENT_WX_QUERY_JSAPI, event -> {
            QueryReq req = (QueryReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = wechatPayJSAPI.doQuery(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("WechatPayWork.query.jsapi exception: {}", e);
                exceptionHandler(e, event);
            }

        });
        //微信H5查询
        vertx.eventBus().consumer(EventConstant.EVENT_WX_QUERY_H5, event -> {
            QueryReq req = (QueryReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = wechatPayH5.doQuery(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("WechatPayWork.query.h5 exception: {}", e);
                exceptionHandler(e, event);
            }

        });
        //微信APP查询
        vertx.eventBus().consumer(EventConstant.EVENT_WX_QUERY_APP, event -> {
            QueryReq req = (QueryReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = wechatPayAPP.doQuery(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("WechatPayWork.query.app exception: {}", e);
                exceptionHandler(e, event);
            }

        });


        //微信Native退款
        vertx.eventBus().consumer(EventConstant.EVENT_WX_REFUND_NATIVE, event -> {
            RefundReq req = (RefundReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = wechatPayNative.doRefund(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("WechatPayWork.refund.native exception: {}", e);
                exceptionHandler(e, event);
            }

        });
        //微信JSAPI退款
        vertx.eventBus().consumer(EventConstant.EVENT_WX_REFUND_JSAPI, event -> {
            RefundReq req = (RefundReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = wechatPayJSAPI.doRefund(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("WechatPayWork.refund.jsapi exception: {}", e);
                exceptionHandler(e, event);
            }

        });
        //微信H5退款
        vertx.eventBus().consumer(EventConstant.EVENT_WX_REFUND_H5, event -> {
            RefundReq req = (RefundReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = wechatPayH5.doRefund(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("WechatPayWork.refund.h5 exception: {}", e);
                exceptionHandler(e, event);
            }

        });
        //微信APP退款
        vertx.eventBus().consumer(EventConstant.EVENT_WX_REFUND_APP, event -> {
            RefundReq req = (RefundReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = wechatPayAPP.doRefund(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("WechatPayWork.refund.app exception: {}", e);
                exceptionHandler(e, event);
            }

        });


        //微信Native退款查询
        vertx.eventBus().consumer(EventConstant.EVENT_WX_REFUND_QUERY_NATIVE, event -> {
            RefundQueryReq req = (RefundQueryReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = wechatPayNative.doRefundQuery(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("WechatPayWork.refund.query.native exception: {}", e);
                exceptionHandler(e, event);
            }

        });
        //微信JSAPI退款查询
        vertx.eventBus().consumer(EventConstant.EVENT_WX_REFUND_QUERY_JSAPI, event -> {
            RefundQueryReq req = (RefundQueryReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = wechatPayJSAPI.doRefundQuery(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("WechatPayWork.refund.query.jsapi exception: {}", e);
                exceptionHandler(e, event);
            }

        });
        //微信H5退款查询
        vertx.eventBus().consumer(EventConstant.EVENT_WX_REFUND_QUERY_H5, event -> {
            RefundQueryReq req = (RefundQueryReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = wechatPayH5.doRefundQuery(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("WechatPayWork.refund.query.h5 exception: {}", e);
                exceptionHandler(e, event);
            }

        });
        //微信APP退款查询
        vertx.eventBus().consumer(EventConstant.EVENT_WX_REFUND_QUERY_APP, event -> {
            RefundQueryReq req = (RefundQueryReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = wechatPayAPP.doRefundQuery(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("WechatPayWork.refund.query.app exception: {}", e);
                exceptionHandler(e, event);
            }

        });


        //微信企业付款到零钱
        vertx.eventBus().consumer(EventConstant.EVENT_WX_ENTER_PAY_CHANGE, event -> {
            PayChangeReq req = (PayChangeReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = wechatPayJSAPI.doEnterPayChange(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("WechatPayWork.enter.pay.change exception: {}", e);
                exceptionHandler(e, event);
            }
        });


        //微信公众号签约
        vertx.eventBus().consumer(EventConstant.EVENT_WX_CONTRACT_JSAPI, event -> {
            ContractReq req = (ContractReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = wechatPayJSAPI.doContract(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("WechatPayWork.contract.jsapi exception: {}", e);
                exceptionHandler(e, event);
            }
        });

        //微信APP签约
        vertx.eventBus().consumer(EventConstant.EVENT_WX_CONTRACT_APP, event -> {
            ContractReq req = (ContractReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = wechatPayAPP.doContract(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("WechatPayWork.contract.app exception: {}", e);
                exceptionHandler(e, event);
            }
        });

        //微信签约查询
        vertx.eventBus().consumer(EventConstant.EVENT_WX_CONTRACT_QUERY, event -> {
            ContractQueryReq req = (ContractQueryReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = wechatPayJSAPI.doContractQuery(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("WechatPayWork.contract.query exception: {}", e);
                exceptionHandler(e, event);
            }
        });

        //微信解约
        vertx.eventBus().consumer(EventConstant.EVENT_WX_TERMINATION, event -> {
            TerminationReq req = (TerminationReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = wechatPayJSAPI.doTermination(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("WechatPayWork.termination exception: {}", e);
                exceptionHandler(e, event);
            }
        });

        //微信代扣
        vertx.eventBus().consumer(EventConstant.EVENT_WX_WITHHOLD, event -> {
            WithholdReq req = (WithholdReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = wechatPayJSAPI.doWithhold(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("WechatPayWork.withhold exception: {}", e);
                exceptionHandler(e, event);
            }
        });

    }
}
