package com.jnc.pay.biz.common.handler;

import com.jnc.pay.biz.common.vo.*;
import com.jnc.pay.constant.BizConstant;
import com.jnc.pay.constant.RedisConstant;
import com.jnc.pay.constant.SysConstant;
import com.jnc.pay.constant.UriConstant;
import com.jnc.pay.core.model.BaseResp;
import com.jnc.pay.core.model.RespCode;
import com.jnc.pay.core.vertx.verticle.WorkerVerticle;
import com.jnc.pay.util.HttpUtil;
import com.jnc.pay.util.ValidatorUtil;
import com.jnc.pay.util.convert.JsonUtil;
import com.jnc.pay.util.gen.IdGen;
import io.vertx.ext.web.Router;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @Auther: jjn
 * @Date: 2020/8/3
 * @Desc:
 */
@Slf4j
@Order(3)
@Component
public class WechatHandler extends WorkerVerticle {

    @Autowired
    private Router router;
    @Value("${pay.deployUrl}")
    private String platformNotifyUrl;

    @Override
    public void start() throws Exception {
        //微信企业付款到零钱 The enterprise pays the change
        router.post("/v1/biz/epc").handler(routingContext -> {
            BaseResp resp = new BaseResp();
            PayChangeReq req = JsonUtil.json2Bean(routingContext.getBodyAsString(SysConstant.CHARSET_NAME), PayChangeReq.class);
            req.setParams(BizConstant.REQ_KEY, routingContext.request());
            //1、hibernator-validator常规参数校验
            resp = ValidatorUtil.valid(req);
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }
            //2、特定支付渠道、支付类型参数校验
            resp = ValidatorUtil.payChangeValid(req);
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }

            //3、获取分发路径
            resp = getPath(RedisConstant.DICT_TRANS_TYPE_EVENT, req);
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }

            //4、校验该appId，是否存在有效的渠道配置信息
            resp = getConfig(req);
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }

            //5、生成转账ID
            req.setTransId(IdGen.getId());

            //6、Rx发布
            rxSub(req);
        });

        //签约
        router.post("/v1/biz/cw").handler(routingContext -> {
            BaseResp resp = new BaseResp();
            ContractReq req = JsonUtil.json2Bean(routingContext.getBodyAsString(SysConstant.CHARSET_NAME), ContractReq.class);
            req.setParams(BizConstant.REQ_KEY, routingContext.request());
            //1、hibernator-validator常规参数校验
            resp = ValidatorUtil.valid(req);
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }
            //2、特定支付渠道、支付类型参数校验
            //resp = ;
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }

            //3、获取分发路径
            resp = getPath(RedisConstant.DICT_CONTRACT_TYPE_EVENT, req);
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }

            //4、校验该appId，是否存在有效的渠道配置信息
            resp = getConfig(req);
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }

            //5、生成支付交易ID和设置平台回调url
            req.setContractId(IdGen.getId());
            setContractPlatformNotifyUrl(req);

            //6、Rx发布
            rxSub(req);
        });

        //解约
        router.post("/v1/biz/tm").handler(routingContext -> {
            BaseResp resp = new BaseResp();
            TerminationReq req = JsonUtil.json2Bean(routingContext.getBodyAsString(SysConstant.CHARSET_NAME), TerminationReq.class);
            req.setParams(BizConstant.REQ_KEY, routingContext.request());
            //1、hibernator-validator常规参数校验
            resp = ValidatorUtil.valid(req);
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }
            //2、特定支付渠道、支付类型参数校验
            //resp = ;
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }

            //3、获取分发路径
            resp = getPath(RedisConstant.DICT_TERMINATION_TYPE_EVENT, req);
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }

            //4、校验该appId，是否存在有效的渠道配置信息
            resp = getConfig(req);
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }

            //6、Rx发布
            rxSub(req);
        });

        //签约、解约状态查询
        router.post("/v1/biz/cwq").handler(routingContext -> {
            BaseResp resp = new BaseResp();
            ContractQueryReq req = JsonUtil.json2Bean(routingContext.getBodyAsString(SysConstant.CHARSET_NAME), ContractQueryReq.class);
            req.setParams(BizConstant.REQ_KEY, routingContext.request());
            //1、hibernator-validator常规参数校验
            resp = ValidatorUtil.valid(req);
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }
            //2、特定支付渠道、支付类型参数校验
            //resp = ;
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }

            //3、获取分发路径
            resp = getPath(RedisConstant.DICT_CONTRACT_QUERY_TYPE_EVENT, req);
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }

            //4、校验该appId，是否存在有效的渠道配置信息
            resp = getConfig(req);
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }

            //6、Rx发布
            rxSub(req);
        });

        //代扣
        router.post("/v1/biz/wh").handler(routingContext -> {
            BaseResp resp = new BaseResp();
            WithholdReq req = JsonUtil.json2Bean(routingContext.getBodyAsString(SysConstant.CHARSET_NAME), WithholdReq.class);
            req.setParams(BizConstant.REQ_KEY, routingContext.request());
            //1、hibernator-validator常规参数校验
            resp = ValidatorUtil.valid(req);
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }
            //2、特定支付渠道、支付类型参数校验
            //resp = ;
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }

            //3、获取分发路径
            resp = getPath(RedisConstant.DICT_WITHHOLD_TYPE_EVENT, req);
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }

            //4、校验该appId，是否存在有效的渠道配置信息
            resp = getConfig(req);
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }

            //5、生成支付交易ID和设置平台回调url
            req.setTradeId(IdGen.getId());
            req.setPlatformNotifyUrl(platformNotifyUrl + UriConstant.WECHAT_WITHHOLD_CALLBACK_V1);

            //6、Rx发布
            rxSub(req);
        });
    }




    /**
     * 设置签约平台回调接口
     * @param req
     */
    private void setContractPlatformNotifyUrl(ContractReq req){
        switch (req.getChannelId()){
            case BizConstant.CHANNEL_ID_WX:
                req.setPlatformNotifyUrl(platformNotifyUrl + UriConstant.WECHAT_CONTRACT_CALLBACK_V1);
                break;
            case BizConstant.CHANNEL_ID_ALI:
                //req.setPlatformNotifyUrl(platformNotifyUrl + UriConstant.);
                break;
        }
    }
}
