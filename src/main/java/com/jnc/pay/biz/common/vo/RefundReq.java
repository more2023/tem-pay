package com.jnc.pay.biz.common.vo;

import com.jnc.pay.core.model.BaseReq;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Auther: jjn
 * @Date: 2020/6/24 17:26
 * @Desc:
 */
@Setter
@Getter
public class RefundReq extends BaseReq {

    /** 应用方退款ID */
    @NotBlank(message = "应用方退款ID不能为空")
    @Length(min = 1, max = 32, message = "应用方退款ID长度不能超过32位")
    private String appRefundId;
    //支付交易ID
    @NotNull(message = "支付交易ID不能为空")
    private Long tradeId;
    /** 退款金额 */
    @NotNull(message = "退款金额不能为空")
    private Integer refundFee;
    /** 金额对应的小数位数 */
    private Integer scale;
    /** 交易的币种 */
    private String currencyCode;
    /** 通知URL */
    @NotBlank(message = "通知URL不能为空")
    private String notifyUrl;
    /** 退款理由 */
    private String refundReason;

    /** =========================  各渠道各支付类型特定字段  ============================= */

    /** 用户标识(如openid，支付宝账号) */
    private String channelUserId;
    /** 额外参数 */
    private String extra;
    /** 客户端IP */
    private String clientIp;

    /** ========================== 程序自定义字段 ============================ */

    //退款ID
    private Long refundId;
    /** 支付金额 */
    private Integer totalFee;
    /** 渠道支付流水号 */
    private String tradeNo;
    /** 应用方订单ID */
    private String appOrderId;
    //平台回调url
    private String platformNotifyUrl;

}
