package com.jnc.pay.biz.common.vo;

import com.jnc.pay.core.model.BaseReq;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * @Auther: jjn
 * @Date: 2020/8/4
 * @Desc: 签约查询
 */
@Setter
@Getter
public class ContractQueryReq extends BaseReq {

    //模板ID
    @NotBlank(message = "模板ID不能为空")
    private String planId;
    //签约协议号
    @NotBlank(message = "签约协议号不能为空")
    private String contractCode;
    //委托代扣协议id不能为空
    private String contractNo;

}
