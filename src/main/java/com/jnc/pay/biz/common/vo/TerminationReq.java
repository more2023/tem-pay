package com.jnc.pay.biz.common.vo;

import com.jnc.pay.core.model.BaseReq;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Auther: jjn
 * @Date: 2020/8/4
 * @Desc: 解约
 */
@Setter
@Getter
public class TerminationReq extends BaseReq {

    //签约ID
    @NotNull(message = "签约ID不能为空")
    private Long contractId;
    //模板ID
    @NotBlank(message = "模板ID不能为空")
    private String planId;
    //签约协议号
    @NotBlank(message = "签约协议号不能为空")
    private String contractCode;
    //解约原因
    @NotBlank(message = "解约原因不能为空")
    private String terminationDesc;
    //委托代扣协议id不能为空
    private String contractNo;


}
