package com.jnc.pay.biz.common.mapper;

import com.jnc.pay.biz.common.model.Notify;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Auther: jjn
 * @Date: 2020/7/6 10:10
 * @Desc: 支付通知Mapper
 */
@Mapper
public interface NotifyMapper {

    /**
     * 新增通知记录
     * @param model
     * @return
     */
    public int addNotify(Notify model);

    /**
     * 修改通知记录
     * @param model
     * @return
     */
    public int editNotify(Notify model);
}
