package com.jnc.pay.biz.common.service.impl;

import com.jnc.pay.biz.common.mapper.WechatpayConfigMapper;
import com.jnc.pay.biz.common.model.WechatpayConfig;
import com.jnc.pay.biz.common.service.WechatpayConfigService;
import com.jnc.pay.constant.RedisConstant;
import com.jnc.pay.core.config.redis.RedisStore;
import com.jnc.pay.util.convert.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Auther: jjn
 * @Date: 2020/5/11 19:20
 * @Desc:
 */
@Slf4j
@Service
public class WechatpayConfigServiceImpl implements WechatpayConfigService {

    @Resource
    private WechatpayConfigMapper wechatpayConfigMapper;
    @Autowired
    private RedisStore redisStore;

    /**
     * 获取有效的appId和wechatpay配置关联List
     * @return
     */
    @Override
    public List<WechatpayConfig> queryWechatpayAppConfig() {
        return wechatpayConfigMapper.queryWechatpayAppConfig();
    }

    /**
     * 根据appId，获取wechatpay配置信息
     * @param appId
     * @return
     */
    @Override
    public WechatpayConfig getWechatpayAppConfig(String appId) {
        return wechatpayConfigMapper.getWechatpayAppConfig(appId);
    }

    /**
     * 加载微信支付配置到redis
     */
    @Override
    public void loadRedisWechatpayConfig() {
        List<WechatpayConfig> list = wechatpayConfigMapper.queryWechatpayAppConfig();
        if(list == null || list.size() < 1){
            log.warn("No valid appId with wechatpay config ...");
            return;
        }
        for (WechatpayConfig obj : list){
            //先清空老的缓存数据
            redisStore.del(RedisConstant.WECHATPAY_CONFIG + obj.getAppId());
            //再存入新的缓存数据
            redisStore.set(RedisConstant.WECHATPAY_CONFIG + obj.getAppId(), JsonUtil.bean2Json(obj));
        }
    }
}
