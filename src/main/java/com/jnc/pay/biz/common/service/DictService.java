package com.jnc.pay.biz.common.service;

import com.jnc.pay.biz.common.model.DictData;

import java.util.List;

/**
 * @Auther: jjn
 * @Date: 2020/4/15 18:54
 * @Desc:
 */
public interface DictService {

    /**
     * 获取指定dictType，有效dictData列表
     * @param dictType
     * @return
     */
    public List<DictData> getData(String dictType);

    /**
     * 加载redis dictData数据
     */
    public void loadRedisDictData();

    /**
     * 根据指定dictType和dictCode，获取dictValue
     * @param dictType
     * @param dictCode
     * @return
     */
    public String getValueBy(String dictType, String dictCode);
}
