package com.jnc.pay.biz.common.work;

import com.jnc.pay.biz.alipay.service.AliPayApp;
import com.jnc.pay.biz.alipay.service.AliPayPage;
import com.jnc.pay.biz.alipay.service.AliPayWap;
import com.jnc.pay.biz.common.vo.PayReq;
import com.jnc.pay.biz.common.vo.QueryReq;
import com.jnc.pay.biz.common.vo.RefundQueryReq;
import com.jnc.pay.biz.common.vo.RefundReq;
import com.jnc.pay.constant.EventConstant;
import com.jnc.pay.core.model.BaseResp;
import com.jnc.pay.core.vertx.verticle.WorkerVerticle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @Auther: jjn
 * @Date: 2020/6/24 17:03
 * @Desc: 支付宝支付work
 */
@Slf4j
@Order(1)
@Component
public class AliPayWork extends WorkerVerticle {

    @Autowired
    private AliPayPage aliPayPage;
    @Autowired
    private AliPayWap aliPayWap;
    @Autowired
    private AliPayApp aliPayApp;

    @Override
    public void start() throws Exception {
        //支付宝PC WEB支付
        vertx.eventBus().consumer(EventConstant.EVENT_ALI_PAY_PAGE, event -> {
            PayReq req = (PayReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = aliPayPage.doPay(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("AliPayWork.pay.page exception: {}", e);
                exceptionHandler(e, event);
            }

        });
        //支付宝Mobile WEB支付
        vertx.eventBus().consumer(EventConstant.EVENT_ALI_PAY_WAP, event -> {
            PayReq req = (PayReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = aliPayWap.doPay(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("AliPayWork.pay.wap exception: {}", e);
                exceptionHandler(e, event);
            }

        });
        //支付宝APP支付
        vertx.eventBus().consumer(EventConstant.EVENT_ALI_PAY_ALIAPP, event -> {
            PayReq req = (PayReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = aliPayApp.doPay(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("AliPayWork.pay.aliapp exception: {}", e);
                exceptionHandler(e, event);
            }

        });

        //支付宝PC WEB查询
        vertx.eventBus().consumer(EventConstant.EVENT_ALI_QUERY_PAGE, event -> {
            QueryReq req = (QueryReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = aliPayPage.doQuery(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("AliPayWork.query.page exception: {}", e);
                exceptionHandler(e, event);
            }

        });
        //支付宝Mobile WEB查询
        vertx.eventBus().consumer(EventConstant.EVENT_ALI_QUERY_WAP, event -> {
            QueryReq req = (QueryReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = aliPayWap.doQuery(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("AliPayWork.query.wap exception: {}", e);
                exceptionHandler(e, event);
            }

        });
        //支付宝APP查询
        vertx.eventBus().consumer(EventConstant.EVENT_ALI_QUERY_ALIAPP, event -> {
            QueryReq req = (QueryReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = aliPayApp.doQuery(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("AliPayWork.query.aliapp exception: {}", e);
                exceptionHandler(e, event);
            }

        });


        //支付宝PC WEB退款
        vertx.eventBus().consumer(EventConstant.EVENT_ALI_REFUND_PAGE, event -> {
            RefundReq req = (RefundReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = aliPayPage.doRefund(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("AliPayWork.refund.page exception: {}", e);
                exceptionHandler(e, event);
            }

        });
        //支付宝Mobile WEB退款
        vertx.eventBus().consumer(EventConstant.EVENT_ALI_REFUND_WAP, event -> {
            RefundReq req = (RefundReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = aliPayWap.doRefund(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("AliPayWork.refund.wap exception: {}", e);
                exceptionHandler(e, event);
            }

        });
        //支付宝APP退款
        vertx.eventBus().consumer(EventConstant.EVENT_ALI_REFUND_ALIAPP, event -> {
            RefundReq req = (RefundReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = aliPayApp.doRefund(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("AliPayWork.refund.aliapp exception: {}", e);
                exceptionHandler(e, event);
            }

        });


        //支付宝PC WEB退款查询
        vertx.eventBus().consumer(EventConstant.EVENT_ALI_REFUND_QUERY_PAGE, event -> {
            RefundQueryReq req = (RefundQueryReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = aliPayPage.doRefundQuery(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("AliPayWork.refund.query.page exception: {}", e);
                exceptionHandler(e, event);
            }

        });
        //支付宝Mobile WEB退款查询
        vertx.eventBus().consumer(EventConstant.EVENT_ALI_REFUND_QUERY_WAP, event -> {
            RefundQueryReq req = (RefundQueryReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = aliPayWap.doRefundQuery(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("AliPayWork.refund.query.wap exception: {}", e);
                exceptionHandler(e, event);
            }

        });
        //支付宝APP退款查询
        vertx.eventBus().consumer(EventConstant.EVENT_ALI_REFUND_QUERY_ALIAPP, event -> {
            RefundQueryReq req = (RefundQueryReq) event.body();
            try{
                BaseResp<Map<String, Object>> resp = aliPayApp.doRefundQuery(req);
                event.reply(resp, getDeliveryOptions());
                return;
            }catch (Exception e){
                log.error("AliPayWork.refund.query.aliapp exception: {}", e);
                exceptionHandler(e, event);
            }

        });
    }
}
