package com.jnc.pay.biz.common.mapper;

import com.jnc.pay.biz.common.model.Trans;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Auther: jjn
 * @Date: 2020/7/3
 * @Desc:
 */
@Mapper
public interface TransMapper {
    /**
     * 新增转账订单
     * @param model
     * @return
     */
    public int addTrans(Trans model);

    /**
     * 根据ID，获取转账订单详情
     * @param transId
     * @return
     */
    public Trans getTransById(Long transId);

    /**
     * 编辑转账订单
     * @param model
     * @return
     */
    public int editTrans(Trans model);

    /**
     * 根据appId和appOrderId，查询转账订单的数量
     * @param model
     * @return
     */
    public Integer queryCount(Trans model);
}
