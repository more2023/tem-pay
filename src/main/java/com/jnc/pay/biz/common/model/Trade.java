package com.jnc.pay.biz.common.model;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 支付流水表 pay_trade
 * @author jjn
 * @date 2020-07-03 11:00:13
 */
@Setter
@Getter
public class Trade {
	/** 本次交易唯一id，整个支付系统唯一，生成他的原因主要是 order_id对于其它应用来说可能重复 */
	private Long tradeId;
	/** 应用方订单号 */
	private String appOrderId;
	/** 应用id */
	private String appId;
	/** 支付渠道ID(如：alipay、wechatpay、unionpay) */
	private String channelId;
	/** 支付类型，比如：微信支付[JSAPI、NATIVE、APP、MWEB] */
	private String payType;
	/** 支付金额，整数方式保存 */
	private Integer totalFee;
	/** 金额对应的小数位数 */
	private Integer scale;
	/** 交易的币种 */
	private String currencyCode;
	/** 渠道用户标识,如微信openId,支付宝账号 */
	private String channelUserId;
	/** 特定渠道发起时额外参数 */
	private String extra;
	/** 支付后，异步通知url */
	private String notifyUrl;
	/** 支付后跳转url */
	private String returnUrl;
	/** 商品描述信息 */
	private String body;
	/** 客户端ip */
	private String clientIp;
	/** 订单过期时间 */
	private Integer expireTime;
	/** 第三方支付成功的时间 */
	private Integer paymentTime;
	/** 通知上游系统的时间 */
	private Integer finishTime;
	/** 第三方的交易流水号（异步通知的时候更新） */
	private String tradeNo;
	/** 0:等待支付，1:完成支付， 2:业务处理完成，3:该笔交易已关闭，-1:支付失败 */
	private Integer orderStatus;
	/** 渠道支付错误码 */
	private String erorrCode;
	/** 渠道支付错误描述 */
	private String erorrMsg;
	/** 扩展参数1 */
	private String param1;
	/** 扩展参数2 */
	private String param2;
	/** 创建时间 */
	private Integer createTime;
	/** 更新时间 */
	private Integer updateTime;
	/** 签约ID */
	private Long contractId;

	/** 原始订单状态 */
	private Integer oldOrderStatus;

    public String toString() {
        return new ToStringBuilder(this)
            .append("tradeId", getTradeId())
            .append("appOrderId", getAppOrderId())
            .append("appId", getAppId())
            .append("channelId", getChannelId())
            .append("payType", getPayType())
            .append("totalFee", getTotalFee())
            .append("scale", getScale())
            .append("currencyCode", getCurrencyCode())
            .append("channelUserId", getChannelUserId())
            .append("extra", getExtra())
            .append("notifyUrl", getNotifyUrl())
            .append("returnUrl", getReturnUrl())
            .append("body", getBody())
            .append("clientIp", getClientIp())
			.append("contractId", getContractId())
            .append("expireTime", getExpireTime())
            .append("paymentTime", getPaymentTime())
            .append("finishTime", getFinishTime())
            .append("tradeNo", getTradeNo())
            .append("orderStatus", getOrderStatus())
            .append("erorrCode", getErorrCode())
            .append("erorrMsg", getErorrMsg())
            .append("param1", getParam1())
            .append("param2", getParam2())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
