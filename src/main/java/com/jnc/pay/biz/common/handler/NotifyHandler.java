package com.jnc.pay.biz.common.handler;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.XmlUtil;
import com.jnc.pay.biz.common.service.WechatpayConfigService;
import com.jnc.pay.constant.*;
import com.jnc.pay.core.vertx.verticle.WorkerVerticle;
import io.vertx.ext.web.Router;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.util.Iterator;
import java.util.Map;

/**
 * @Auther: jjn
 * @Date: 2020/7/2 10:58
 * @Desc: 回调Handler
 */
@Slf4j
@Order(3)
@Component
public class NotifyHandler extends WorkerVerticle {

    @Autowired
    private Router router;
    @Autowired
    WechatpayConfigService wechatpayConfigService;

    @Override
    public void start() throws Exception {

        //微信支付回调通知
        router.post(UriConstant.WECHAT_PAY_CALLBACK_V1).handler(routingContext -> {
            Map<String, Object> respMap = MapUtil.newHashMap();
            String xmlStr = XmlUtil.format(XmlUtil.readXML(new ByteArrayInputStream(routingContext.getBody().getBytes())));
            log.debug("wechatpay pay callback, req: {}", xmlStr);

            vertx.eventBus().send(EventConstant.EVENT_WX_PAY_CALLBACK_NOTIFY, xmlStr, getDeliveryOptions());

            //同步返回微信响应
            respMap.put("return_code", "SUCCESS");
            respMap.put("return_msg", "OK");
            String xmlResp = XmlUtil.mapToXmlStr(respMap);
            routingContext.request().response().end(xmlResp);
        }).failureHandler(routingContext -> {
            log.error("Wechat Payment Callback Internal Server Error: {}", routingContext.failure());
        });

        //微信退款回调通知
        router.post(UriConstant.WECHAT_REFUND_CALLBACK_V1).handler(routingContext -> {
            Map<String, Object> respMap = MapUtil.newHashMap();
            String xmlStr = XmlUtil.format(XmlUtil.readXML(new ByteArrayInputStream(routingContext.getBody().getBytes())));
            log.debug("wechat refund callback, req: {}", xmlStr);

            vertx.eventBus().send(EventConstant.EVENT_WX_REFUND_CALLBACK_NOTIFY, xmlStr, getDeliveryOptions());

            //同步返回微信响应
            respMap.put("return_code", "SUCCESS");
            respMap.put("return_msg", "OK");
            String xmlResp = XmlUtil.mapToXmlStr(respMap);
            routingContext.request().response().end(xmlResp);
        }).failureHandler(routingContext -> {
            log.error("Wechat Refund Callback Internal Server Error: {}", routingContext.failure());
        });

        //微信签约、解约回调通知
        router.post(UriConstant.WECHAT_CONTRACT_CALLBACK_V1).handler(routingContext -> {
            Map<String, Object> respMap = MapUtil.newHashMap();
            String xmlStr = XmlUtil.format(XmlUtil.readXML(new ByteArrayInputStream(routingContext.getBody().getBytes())));
            log.debug("wechat contract and termination callback, req: {}", xmlStr);

            vertx.eventBus().send(EventConstant.EVENT_WX_REFUND_CALLBACK_NOTIFY, xmlStr, getDeliveryOptions());

            //同步返回微信响应
            respMap.put("return_code", "SUCCESS");
            respMap.put("return_msg", "OK");
            String xmlResp = XmlUtil.mapToXmlStr(respMap);
            routingContext.request().response().end(xmlResp);
        }).failureHandler(routingContext -> {
            log.error("Wechat contract and termination Callback Internal Server Error: {}", routingContext.failure());
        });

        //微信代扣回调通知
        router.post(UriConstant.WECHAT_WITHHOLD_CALLBACK_V1).handler(routingContext -> {
            Map<String, Object> respMap = MapUtil.newHashMap();
            String xmlStr = XmlUtil.format(XmlUtil.readXML(new ByteArrayInputStream(routingContext.getBody().getBytes())));
            log.debug("wechatpay withhold callback, req: {}", xmlStr);

            vertx.eventBus().send(EventConstant.EVENT_WX_WITHHOLD_CALLBACK_NOTIFY, xmlStr, getDeliveryOptions());

            //同步返回微信响应
            respMap.put("return_code", "SUCCESS");
            respMap.put("return_msg", "OK");
            String xmlResp = XmlUtil.mapToXmlStr(respMap);
            routingContext.request().response().end(xmlResp);
        }).failureHandler(routingContext -> {
            log.error("Wechat withhold Callback Internal Server Error: {}", routingContext.failure());
        });

        //支付宝支付和退款回调通知
        router.post(UriConstant.ALI_PAY_CALLBACK_V1).handler(routingContext -> {
            Map<String, String> map = MapUtil.newHashMap();
            Iterator<Map.Entry<String, String>> it = routingContext.request().params().iterator();
            while (it.hasNext()){
                Map.Entry<String, String> next = it.next();
                map.put(next.getKey(), next.getValue());
            }

            vertx.eventBus().send(EventConstant.EVENT_ALI_CALLBACK_NOTIFY, map, getDeliveryOptions());

            routingContext.request().response().end("success");
        });
    }

}
