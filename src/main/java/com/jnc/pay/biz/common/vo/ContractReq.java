package com.jnc.pay.biz.common.vo;

import com.jnc.pay.core.model.BaseReq;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @Auther: jjn
 * @Date: 2020/8/4
 * @Desc: 签约
 */
@Setter
@Getter
public class ContractReq extends BaseReq {
    //模板ID
    @NotBlank(message = "模板ID不能为空")
    private String planId;
    //签约协议号
    @NotBlank(message = "签约协议号不能为空")
    private String contractCode;
    //用户账户展示名称
    @NotBlank(message = "用户账户展示名称不能为空")
    private String contractName;
    //回调通知URL
    private String notifyUrl;





    /** 额外参数 */
    private String extra;
    /** 客户端IP */
    private String clientIp;

    //签约ID
    private Long contractId;
    //平台回调url
    private String platformNotifyUrl;

}
