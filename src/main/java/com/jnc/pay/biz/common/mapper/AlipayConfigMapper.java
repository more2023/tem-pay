package com.jnc.pay.biz.common.mapper;

import com.jnc.pay.biz.common.model.AlipayConfig;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Auther: jjn
 * @Date: 2020/5/11 19:17
 * @Desc:
 */
@Mapper
public interface AlipayConfigMapper {

    public List<AlipayConfig> queryAlipayAppConfig();

    public AlipayConfig getAlipayAppConfig(String appId);
}
