package com.jnc.pay.biz.common.service;

import com.jnc.pay.biz.common.model.AlipayConfig;

import java.util.List;

/**
 * @Auther: jjn
 * @Date: 2020/5/11 19:19
 * @Desc:
 */
public interface AlipayConfigService {

    /**
     * 获取有效的appId和alipay配置关联List
     * @return
     */
    public List<AlipayConfig> queryAlipayAppConfig();

    /**
     * 根据appId，获取alipay配置信息
     * @param appId
     * @return
     */
    public AlipayConfig getAlipayAppConfig(String appId);

    /**
     * 加载支付宝支付配置到redis
     */
    public void loadRedisAlipayConfig();
}
