package com.jnc.pay.biz.common.mapper;

import com.jnc.pay.biz.common.model.Mch;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Auther: jjn
 * @Date: 2020/7/6 10:10
 * @Desc: 商户Mapper
 */
@Mapper
public interface MchMapper {

    /**
     * 获取有效商户列表
     * @return
     */
    public List<Mch> queryMch();

    /**
     * 根据id，查询有效的商户详细信息
     * @param mchId
     * @return
     */
    public Mch queryMchById(Long mchId);

}
