package com.jnc.pay.biz.common.service;

import com.jnc.pay.biz.common.model.Mch;


/**
 * @Auther: jjn
 * @Date: 2020/4/15 18:54
 * @Desc: 商户Service
 */
public interface MchService {

    /**
     * 加载redis mch数据
     */
    public void loadRedisMch();

    /**
     * 根据商户ID，获取商户信息(先从redis取，redis取不到再到数据库取)
     * @param mchId
     * @return
     */
    public Mch getMchBy(Long mchId);
}
