package com.jnc.pay.biz.common.mapper;

import com.jnc.pay.biz.common.model.WechatpayConfig;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Auther: jjn
 * @Date: 2020/5/11 19:17
 * @Desc:
 */
@Mapper
public interface WechatpayConfigMapper {

    public List<WechatpayConfig> queryWechatpayAppConfig();

    public WechatpayConfig getWechatpayAppConfig(String appId);

    public WechatpayConfig queryConfigBy(WechatpayConfig model);
}
