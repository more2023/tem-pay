package com.jnc.pay.biz.common.service;

import com.jnc.pay.biz.common.vo.*;
import com.jnc.pay.core.model.BaseResp;

import java.util.Map;

/**
 * @Auther: jjn
 * @Date: 2020/6/24 17:17
 * @Desc: 支付处理service
 */
public interface PaymentService {

    /**
     * 支付业务处理
     * @param req
     * @return
     */
    public BaseResp<Map<String, Object>> doPay(PayReq req);

    /**
     * 支付状态查询
     * @param req
     * @return
     */
    public BaseResp<Map<String, Object>> doQuery(QueryReq req);

    /**
     * 退款业务处理
     * @param req
     * @return
     */
    public BaseResp<Map<String, Object>> doRefund(RefundReq req);

    /**
     * 退款状态查询
     * @param req
     * @return
     */
    public BaseResp<Map<String, Object>> doRefundQuery(RefundQueryReq req);

}
