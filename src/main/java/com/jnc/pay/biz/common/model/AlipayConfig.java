package com.jnc.pay.biz.common.model;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 支付宝支付配置表 pay_ali_config
 * @author jjn
 * @date 2020-05-11
 */
@Setter
@Getter
public class AlipayConfig {
	/** ID */
	private Long id;
	/** 合作伙伴身份ID */
	private String pid;
	/** 支付宝企业账户(邮箱) */
	private String sellerId;
	/** 支付宝应用ID */
	private String alipayAppId;
	/** MD5秘钥 */
	private String md5Secret;
	/** 加密方式 */
	private String signType;
	/** 支付宝公钥 */
	private String publicKey;
	/** 支付宝应用私钥 */
	private String privateKey;
	/** 状态 0:正常, 1:禁用, -1:删除 */
	private Integer status;
	/** 创建时间 */
	private Integer createTime;
	/** 创建人id */
	private Integer createBy;
	/** 更新时间 */
	private Integer updateTime;
	/** 修改人id */
	private Integer updateBy;

	/** 应用ID */
	private String appId;

    public String toString() {
        return new ToStringBuilder(this)
            .append("id", getId())
            .append("pid", getPid())
            .append("sellerId", getSellerId())
            .append("alipayAppId", getAlipayAppId())
            .append("md5Secret", getMd5Secret())
            .append("signType", getSignType())
            .append("publicKey", getPublicKey())
            .append("privateKey", getPrivateKey())
            .append("status", getStatus())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
