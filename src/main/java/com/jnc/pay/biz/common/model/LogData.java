package com.jnc.pay.biz.common.model;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * @Auther: jjn
 * @Date: 2020/6/22 15:44
 * @Desc: 交易日志表 pay_log
 */
@Setter
@Getter
public class LogData {
    /** 日志ID */
    private Long logId;
    /** 应用id */
    private String appId;
    /** 应用方订单号 */
    private String appOrderId;
    /** 日志类型，payment:支付; refund:退款; notify:异步通知; query:查询 */
    private String logType;
    /** 请求的header 头 */
    private String requestHeader;
    /** 支付的请求参数 */
    private String requestParams;
    /** 创建时间 */
    private Integer createTime;
    /** 客户端ip */
    private String clientIp;

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("logId", getLogId())
                .append("appId", getAppId())
                .append("appOrderId", getAppOrderId())
                .append("logType", getLogType())
                .append("requestHeader", getRequestHeader())
                .append("requestParams", getRequestParams())
                .append("createTime", getCreateTime())
                .append("clientIp", getClientIp())
                .toString();
    }
}
