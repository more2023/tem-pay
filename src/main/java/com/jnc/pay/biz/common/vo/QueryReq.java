package com.jnc.pay.biz.common.vo;

import com.jnc.pay.core.model.BaseReq;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Auther: jjn
 * @Date: 2020/6/24 17:26
 * @Desc:
 */
@Setter
@Getter
public class QueryReq extends BaseReq {

    /** 应用方订单ID */
    @NotBlank(message = "应用方订单ID不能为空")
    @Length(min = 1, max = 32, message = "应用方订单ID长度不能超过32位")
    private String appOrderId;
    //交易ID(程序生成)
    @NotNull(message = "交易ID不能为空")
    private Long tradeId;

}
