package com.jnc.pay.biz.common.model;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 上游通知表 pay_notify
 * @author jjn
 * @date 2020-07-06 10:06:48
 */
@Setter
@Getter
public class Notify {
	/** 通知ID */
	private Long notifyId;
	/** 应用id */
	private String appId;
	/** 交易id */
	private Long tradeId;
	/** 应用方订单号 */
	private String appOrderId;
	/** 通知类型:pay、refund、canceled */
	private String notifyType;
	/** 通知地址 */
	private String notifyUrl;
	/** 通知次数 */
	private Integer notifyCount;
	/** 通知状态,1-通知中,2-通知成功,3-通知失败 */
	private Integer notifyStatus;
	/** 最后一次通知时间 */
	private Integer lastNotifyTime;
	/** 创建时间 */
	private Integer createTime;
	/** 更新时间 */
	private Integer updateTime;

    public String toString() {
        return new ToStringBuilder(this)
            .append("notifyId", getNotifyId())
            .append("appId", getAppId())
            .append("tradeId", getTradeId())
            .append("appOrderId", getAppOrderId())
            .append("notifyType", getNotifyType())
            .append("notifyUrl", getNotifyUrl())
            .append("notifyCount", getNotifyCount())
            .append("notifyStatus", getNotifyStatus())
            .append("lastNotifyTime", getLastNotifyTime())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
