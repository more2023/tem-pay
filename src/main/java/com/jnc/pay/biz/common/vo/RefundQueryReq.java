package com.jnc.pay.biz.common.vo;

import com.jnc.pay.core.model.BaseReq;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Auther: jjn
 * @Date: 2020/6/24 17:26
 * @Desc:
 */
@Setter
@Getter
public class RefundQueryReq extends BaseReq {

    /** 应用方退款ID */
    @NotBlank(message = "应用方退款ID不能为空")
    private String appRefundId;
    //退款交易ID
    @NotNull(message = "退款交易ID不能为空")
    private Long refundId;


    /** =========================  各渠道各支付类型特定字段  ============================= */

    private Long tradeId;

    private String tradeNo;


}
