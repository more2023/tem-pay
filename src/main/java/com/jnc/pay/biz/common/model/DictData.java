package com.jnc.pay.biz.common.model;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Date;

@Setter
@Getter
public class DictData {
    /** 字典数据主键 */
    private Integer dataId;
    /** 字典类型 */
    private String dictType;
    /** 字典编码 */
    private String dictCode;
    /** 字典值 */
    private String dictValue;
    /** 字典排序 */
    private Integer dictSort;
    /** 样式属性 */
    private String cssClass;
    /** 是否默认（Y是 N否） */
    private String isDefault;
    /** 状态（0正常 1停用） */
    private Integer status;
    /** 创建时间 */
    private Date createTime;
    /** 备注 */
    private String remark;
    /** 字典名称 */
    private String dictName;


    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("dataId", getDataId())
                .append("dictType", getDictType())
                .append("dictCode", getDictCode())
                .append("dictValue", getDictValue())
                .append("dictSort", getDictSort())
                .append("cssClass", getCssClass())
                .append("isDefault", getIsDefault())
                .append("status", getStatus())
                .append("createTime", getCreateTime())
                .append("remark", getRemark())
                .toString();
    }
}