package com.jnc.pay.biz.common.model;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 退款流水表 pay_refund
 * @author jjn
 * @date 2020-07-03 11:00:13
 */
@Setter
@Getter
public class Refund {
	/** 退款id */
	private Long refundId;
	/** 交易id */
	private Long tradeId;
	/** 应用方退款id */
	private String appRefundId;
	/** 第三方的流水号 */
	private String tradeNo;
	/** 应用id */
	private String appId;
	/** 支付渠道ID(如：alipay、wechatpay、unionpay) */
	private String channelId;
	/** 支付类型，比如：微信支付[JSAPI、NATIVE、APP、MWEB] */
	private String payType;
	/** 退款金额，整数方式保存 */
	private Integer refundFee;
	/** 金额对应的小数位数 */
	private Integer scale;
	/** 交易的币种(CNY、USD、HKD) */
	private String currencyCode;
	/** 渠道用户标识,如微信openId,支付宝账号 */
	private String channelUserId;
	/** 特定渠道发起时额外参数 */
	private String extra;
	/** 退款后，异步通知url */
	private String notifyUrl;
	/** 第三方退款流水号 */
	private String refundNo;
	/** 订单过期时间 */
	private Integer expireTime;
	/** 第三方退款成功的时间 */
	private Integer refundTime;
	/** 通知上游系统的时间 */
	private Integer finishTime;
	/** 退款理由 */
	private String refundReason;
	/** 客户端ip */
	private String clientIp;
	/** 退款类型；0:业务退款; 1:重复退款 */
	private Integer refundType;
	/** 退款方式：1自动原路返回; 2人工打款 */
	private Integer refundWay;
	/** 0未退款; 1退款处理中; 2退款成功; 3该笔交易已关闭; -1退款不成功 */
	private Integer refundStatus;
	/** 渠道支付错误码 */
	private String erorrCode;
	/** 渠道支付错误描述 */
	private String erorrMsg;
	/** 扩展参数1 */
	private String param1;
	/** 扩展参数2 */
	private String param2;
	/** 创建时间 */
	private Integer createTime;
	/** 更新时间 */
	private Integer updateTime;

	/** 原始退款状态 */
	private Integer oldRefundStatus;

    public String toString() {
        return new ToStringBuilder(this)
            .append("refundId", getRefundId())
            .append("tradeId", getTradeId())
            .append("appRefundId", getAppRefundId())
            .append("tradeNo", getTradeNo())
            .append("appId", getAppId())
            .append("channelId", getChannelId())
            .append("payType", getPayType())
            .append("refundFee", getRefundFee())
            .append("scale", getScale())
            .append("currencyCode", getCurrencyCode())
            .append("channelUserId", getChannelUserId())
            .append("extra", getExtra())
            .append("notifyUrl", getNotifyUrl())
			.append("refundNo", getRefundNo())
            .append("expireTime", getExpireTime())
            .append("refundTime", getRefundTime())
            .append("finishTime", getFinishTime())
            .append("refundReason", getRefundReason())
            .append("clientIp", getClientIp())
            .append("refundType", getRefundType())
            .append("refundWay", getRefundWay())
            .append("refundStatus", getRefundStatus())
            .append("erorrCode", getErorrCode())
            .append("erorrMsg", getErorrMsg())
            .append("param1", getParam1())
            .append("param2", getParam2())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
