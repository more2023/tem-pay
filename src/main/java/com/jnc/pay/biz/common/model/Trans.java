package com.jnc.pay.biz.common.model;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 转账流水表 pay_trans
 * @author jjn
 * @date 2020-08-03
 */
@Setter
@Getter
public class Trans {
	/** 转账交易唯一id，整个支付系统唯一 */
	private Long transId;
	/** 应用方转账单号 */
	private String appTransId;
	/** 应用id */
	private String appId;
	/** 支付渠道ID(如：alipay、wechatpay、unionpay) */
	private String channelId;
	/** 支付类型，比如：微信支付[SEP、OEP] */
	private String payType;
	/** 金额，整数方式保存 */
	private Integer totalFee;
	/** 金额对应的小数位数 */
	private Integer scale;
	/** 交易的币种 */
	private String currencyCode;
	/** 渠道用户标识,如微信openId,支付宝账号 */
	private String channelUserId;
	/** 用户姓名 */
	private String userName;
	/** 特定渠道发起时额外参数 */
	private String extra;
	/** 转账后，异步通知url */
	private String notifyUrl;
	/** 转账说明 */
	private String transDesc;
	/** 客户端ip */
	private String clientIp;
	/** 订单过期时间 */
	private Integer expireTime;
	/** 第三方转账成功的时间 */
	private Integer transTime;
	/** 通知上游系统的时间 */
	private Integer finishTime;
	/** 第三方的转账流水号 */
	private String transNo;
	/** 0:待转账，1:转账成功， 2:业务处理完成，3:该笔交易已关闭，-1:转账失败 */
	private Integer transStatus;
	/** 渠道转账错误码 */
	private String erorrCode;
	/** 渠道转账错误描述 */
	private String erorrMsg;
	/** 扩展参数1 */
	private String param1;
	/** 扩展参数2 */
	private String param2;
	/** 创建时间 */
	private Integer createTime;
	/** 更新时间 */
	private Integer updateTime;

	/** 原始转账状态 */
	private Integer oldTransStatus;

    public String toString() {
        return new ToStringBuilder(this)
            .append("transId", getTransId())
            .append("appTransId", getAppTransId())
            .append("appId", getAppId())
            .append("channelId", getChannelId())
            .append("payType", getPayType())
            .append("totalFee", getTotalFee())
            .append("scale", getScale())
            .append("currencyCode", getCurrencyCode())
            .append("channelUserId", getChannelUserId())
            .append("userName", getUserName())
            .append("extra", getExtra())
            .append("notifyUrl", getNotifyUrl())
            .append("transDesc", getTransDesc())
            .append("clientIp", getClientIp())
            .append("expireTime", getExpireTime())
            .append("transTime", getTransTime())
            .append("finishTime", getFinishTime())
            .append("transNo", getTransNo())
            .append("transStatus", getTransStatus())
            .append("erorrCode", getErorrCode())
            .append("erorrMsg", getErorrMsg())
            .append("param1", getParam1())
            .append("param2", getParam2())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
