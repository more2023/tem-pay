package com.jnc.pay.biz.common.mapper;

import com.jnc.pay.biz.common.model.Refund;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Auther: jjn
 * @Date: 2020/7/7 10:01
 * @Desc:
 */
@Mapper
public interface RefundMapper {
    /**
     * 新增退款订单
     * @param model
     * @return
     */
    public int addRefund(Refund model);

    /**
     * 根据ID，获取退款订单详情
     * @param refundId
     * @return
     */
    public Refund getRefundById(Long refundId);

    /**
     * 根据支付交易ID，获取退款订单详情
     * @param tradeId
     * @return
     */
    public Refund getRefundByTradeId(Long tradeId);

    /**
     * 编辑退款订单
     * @param model
     * @return
     */
    public int editRefund(Refund model);

    /**
     * 根据appId、appRefundId，查询退款订单的数据
     * @param model
     * @return
     */
    public Integer queryCount(Refund model);
}
