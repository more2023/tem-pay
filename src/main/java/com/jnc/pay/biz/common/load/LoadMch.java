package com.jnc.pay.biz.common.load;

import com.jnc.pay.biz.common.service.MchService;
import com.jnc.pay.core.config.redis.RedisStore;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;


/**
 * @Author: jjn
 * @Date: 2019/2/27 17:49
 * @Desc: 加载有效商户到redis
 */
@Slf4j
@Component
@Order(value = 1)
public class LoadMch implements CommandLineRunner {

    @Resource
    MchService mchService;
    @Autowired
    RedisStore redisStore;

    @Override
    public void run(String... args) throws Exception {
        log.debug("Start load mch list to redis ... ");
        long start = System.currentTimeMillis();

        //清除旧数据缓存，保存新数据缓存
        mchService.loadRedisMch();

        long end = System.currentTimeMillis();
        log.debug("End load mch list to redis, cost time: {}", end - start);
    }

}
