package com.jnc.pay.biz.common.model;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 商户表 pay_mch
 * @author jjn
 * @date 2020-07-24 17:18:24
 */
@Setter
@Getter
public class Mch {
	
	/** 商户ID */
	private Long mchId;
	/** 商户名称 */
	private String mchName;
	/** 商户类型 */
	private String mchType;
	/** 平台私钥 */
	private String priKey;
	/** 平台公钥 */
	private String pubKey;
	/** 商户公钥 */
	private String mchPubKey;
	/** 状态 0:正常, 1:禁用, -1:删除 */
	private Integer status;
	/** 创建时间 */
	private Integer createTime;
	/** 创建人id */
	private Integer createBy;
	/** 更新时间 */
	private Integer updateTime;
	/** 修改人id */
	private Integer updateBy;

    public String toString() {
        return new ToStringBuilder(this)
            .append("mchId", getMchId())
            .append("mchName", getMchName())
            .append("mchType", getMchType())
            .append("priKey", getPriKey())
            .append("pubKey", getPubKey())
            .append("mchPubKey", getMchPubKey())
            .append("status", getStatus())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
