package com.jnc.pay.biz.common.mapper;

import com.jnc.pay.biz.common.model.DictData;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Auther: jjn
 * @Date: 2020/4/15 18:56
 * @Desc:
 */
@Mapper
public interface DictMapper {

    public List<DictData> queryDictData();
    public List<DictData> getDictDataByType(String dictType);
    public DictData getDictDataValue(DictData record);
    public List<String> queryDictType();
}
