package com.jnc.pay.biz.common.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.map.MapUtil;
import com.jnc.pay.biz.common.mapper.DictMapper;
import com.jnc.pay.biz.common.model.DictData;
import com.jnc.pay.biz.common.service.DictService;
import com.jnc.pay.constant.RedisConstant;
import com.jnc.pay.core.config.redis.RedisStore;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @Auther: jjn
 * @Date: 2020/4/15 18:57
 * @Desc:
 */
@Slf4j
@Service
public class DictServiceImpl implements DictService {

    @Resource
    DictMapper dictMapper;
    @Autowired
    RedisStore redisStore;

    /**
     * 根据字典类型，获取有效字典数据列表
     * @param dictType
     * @return
     */
    @Override
    public List<DictData> getData(String dictType) {
        return dictMapper.getDictDataByType(dictType);
    }

    /**
     * 加载redis dictData数据
     */
    @Override
    public void loadRedisDictData() {
        //获取所有有效的字典值
        List<DictData> dictDataList = dictMapper.queryDictData();
        List<String> dictTypeList = dictMapper.queryDictType();

        if(dictTypeList == null || dictTypeList.size() < 1){
            log.warn("No valid dict type ...");
            return;
        }
        if(dictDataList == null || dictDataList.size() < 1){
            log.warn("No valid dict data ...");
            return;
        }

        for (String type : dictTypeList){
            Map<String, Object> map = MapUtil.newHashMap();
            for (DictData obj : dictDataList){
                if(type.equals(obj.getDictType())){
                    map.put(obj.getDictCode(), obj.getDictValue());
                }
            }
            //先清空老的缓存数据
            redisStore.del(RedisConstant.DICT_DATA + type);
            //再存入新的缓存数据
            redisStore.putHashValues(RedisConstant.DICT_DATA + type, map);
        }
    }

    /**
     * 根据字典类型、字典编码，获取字典值(先从缓存取，缓存没有再从数据库取)
     * @param dictType
     * @param dictCode
     * @return
     */
    @Override
    public String getValueBy(String dictType, String dictCode) {
        String key = RedisConstant.DICT_DATA + dictType;
        String hashValues = (String) redisStore.getHashValues(key, dictCode);
        //为空，则从数据库里取，并设置到redis
        if(StringUtils.isBlank(hashValues)){
            boolean flag = redisStore.exists(key);
            if(flag){
                String value = null;
                DictData model = new DictData();
                model.setDictType(dictType);
                model.setDictCode(dictCode);
                DictData temp = dictMapper.getDictDataValue(model);
                if(temp != null){
                    redisStore.putHashValue(key, dictCode, temp.getDictValue());
                }
            }else{
                List<DictData> list = dictMapper.getDictDataByType(dictType);
                if(CollectionUtil.isEmpty(list)){
                    return hashValues;
                }
                Map<String, Object> map = MapUtil.newHashMap();
                for (DictData obj : list){
                    map.put(obj.getDictCode(), obj.getDictValue());
                    if(dictCode.equals(obj.getDictCode())){
                        hashValues = obj.getDictValue();
                    }
                }
                //先清空老的缓存数据
                redisStore.del(key);
                //再存入新的缓存数据
                redisStore.putHashValues(key, map);
            }
        }
        return hashValues;
    }

}
