package com.jnc.pay.biz.common.load;

import com.jnc.pay.biz.common.service.WechatpayConfigService;
import com.jnc.pay.core.config.redis.RedisStore;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;


/**
 * @Author: jjn
 * @Date: 2019/2/27 17:49
 * @Desc: 加载微信支付配置到redis
 */
@Slf4j
@Component
@Order(value = 1)
public class LoadWechatpayConfig implements CommandLineRunner {

    @Autowired
    WechatpayConfigService wechatpayConfigService;
    @Autowired
    RedisStore redisStore;

    @Override
    public void run(String... args) throws Exception {
        log.debug("Start load wechatpay config to redis ... ");
        long start = System.currentTimeMillis();

        //清除旧数据缓存，保存新数据缓存
        wechatpayConfigService.loadRedisWechatpayConfig();

        long end = System.currentTimeMillis();
        log.debug("End load wechatpay config to redis, cost time: {}", end - start);
    }
}
