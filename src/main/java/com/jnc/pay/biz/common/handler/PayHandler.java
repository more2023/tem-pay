package com.jnc.pay.biz.common.handler;

import cn.hutool.core.util.StrUtil;
import com.jnc.pay.biz.common.model.AlipayConfig;
import com.jnc.pay.biz.common.model.WechatpayConfig;
import com.jnc.pay.biz.common.vo.PayReq;
import com.jnc.pay.biz.common.vo.QueryReq;
import com.jnc.pay.biz.common.vo.RefundQueryReq;
import com.jnc.pay.biz.common.vo.RefundReq;
import com.jnc.pay.constant.BizConstant;
import com.jnc.pay.constant.RedisConstant;
import com.jnc.pay.constant.SysConstant;
import com.jnc.pay.constant.UriConstant;
import com.jnc.pay.core.config.redis.RedisStore;
import com.jnc.pay.core.model.BaseReq;
import com.jnc.pay.core.model.BaseResp;
import com.jnc.pay.core.model.RespCode;
import com.jnc.pay.core.vertx.verticle.WorkerVerticle;
import com.jnc.pay.util.HttpUtil;
import com.jnc.pay.util.ValidatorUtil;
import com.jnc.pay.util.convert.ConvertUtil;
import com.jnc.pay.util.convert.JsonUtil;
import com.jnc.pay.util.gen.IdGen;
import com.jnc.pay.util.pay.WechatPayUtil;
import io.vertx.ext.web.Router;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @Auther: jjn
 * @Date: 2020/7/2 10:58
 * @Desc:
 */
@Slf4j
@Order(3)
@Component
public class PayHandler extends WorkerVerticle {

    @Autowired
    private Router router;
    @Value("${pay.deployUrl}")
    private String platformNotifyUrl;

    @Override
    public void start() throws Exception {
        //支付
        router.post("/v1/biz/pyh").handler(routingContext -> {
            BaseResp resp = new BaseResp();
            PayReq req = JsonUtil.json2Bean(routingContext.getBodyAsString(SysConstant.CHARSET_NAME), PayReq.class);
            req.setParams(BizConstant.REQ_KEY, routingContext.request());
            //1、hibernator-validator常规参数校验
            resp = ValidatorUtil.valid(req);
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }
            //2、特定支付渠道、支付类型参数校验
            resp = ValidatorUtil.payValid(req);
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }

            //3、获取分发路径
            resp = getPath(RedisConstant.DICT_PAY_TYPE_EVENT, req);
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }

            //4、校验该appId，是否存在有效的渠道配置信息
            resp = getConfig(req);
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }

            //5、生成支付交易ID和设置平台回调url
            req.setTradeId(IdGen.getId());
            setPayPlatformNotifyUrl(req);

            //6、Rx发布
            rxSub(req);
        });

        //支付订单查询
        router.post("/v1/biz/pqyh").handler(routingContext -> {
            BaseResp resp = new BaseResp();
            QueryReq req = JsonUtil.json2Bean(routingContext.getBodyAsString(SysConstant.CHARSET_NAME), QueryReq.class);
            req.setParams(BizConstant.REQ_KEY, routingContext.request());
            //1、hibernator-validator常规参数校验
            resp = ValidatorUtil.valid(req);
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }
            //2、特定支付渠道、支付类型参数校验
            //resp = null;
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }

            //3、获取分发路径
            resp = getPath(RedisConstant.DICT_QUERY_TYPE_EVENT, req);
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }

            //4、校验该appId，是否存在有效的渠道配置信息
            resp = getConfig(req);
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }

            //6、Rx发布
            rxSub(req);
        });

        //退款
        router.post("/v1/biz/rfh").handler(routingContext -> {
            BaseResp resp = new BaseResp();
            RefundReq req = JsonUtil.json2Bean(routingContext.getBodyAsString(SysConstant.CHARSET_NAME), RefundReq.class);
            req.setParams(BizConstant.REQ_KEY, routingContext.request());
            //1、hibernator-validator常规参数校验
            resp = ValidatorUtil.valid(req);
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }
            //2、特定支付渠道、支付类型参数校验
            resp = ValidatorUtil.refundValid(req);
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }

            //3、获取分发路径
            resp = getPath(RedisConstant.DICT_REFUND_TYPE_EVENT, req);
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }

            //4、校验该appId，是否存在有效的渠道配置信息
            resp = getConfig(req);
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }

            //5、生成退款ID和设置平台回调url
            req.setRefundId(IdGen.getId());
            setRefundPlatformNotifyUrl(req);

            //6、Rx发布
            rxSub(req);
        });


        //退款订单查询
        router.post("/v1/biz/rqyh").handler(routingContext -> {
            BaseResp resp = new BaseResp();
            RefundQueryReq req = JsonUtil.json2Bean(routingContext.getBodyAsString(SysConstant.CHARSET_NAME), RefundQueryReq.class);
            req.setParams(BizConstant.REQ_KEY, routingContext.request());
            //1、hibernator-validator常规参数校验
            resp = ValidatorUtil.valid(req);
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }
            //2、特定支付渠道、支付类型参数校验
            //resp = null;
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }

            //3、获取分发路径
            resp = getPath(RedisConstant.DICT_REFUND_QUERY_TYPE_EVENT, req);
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }

            //4、校验该appId，是否存在有效的渠道配置信息
            resp = getConfig(req);
            if(resp.getCode() != RespCode.SUCCESS.getCode()){
                HttpUtil.instance.resp(req, resp);
                return;
            }

            //6、Rx发布
            rxSub(req);
        });


    }

    /**
     * 设置支付平台回调接口
     * @param req
     */
    private void setPayPlatformNotifyUrl(PayReq req){
        switch (req.getChannelId()){
            case BizConstant.CHANNEL_ID_WX:
                req.setPlatformNotifyUrl(platformNotifyUrl + UriConstant.WECHAT_PAY_CALLBACK_V1);
                break;
            case BizConstant.CHANNEL_ID_ALI:
                req.setPlatformNotifyUrl(platformNotifyUrl + UriConstant.ALI_PAY_CALLBACK_V1);
                break;
        }
    }

    /**
     * 设置退款平台回调接口
     * @param req
     */
    private void setRefundPlatformNotifyUrl(RefundReq req){
        switch (req.getChannelId()){
            case BizConstant.CHANNEL_ID_WX:
                req.setPlatformNotifyUrl(platformNotifyUrl + UriConstant.WECHAT_REFUND_CALLBACK_V1);
                break;
            case BizConstant.CHANNEL_ID_ALI:
                req.setPlatformNotifyUrl(platformNotifyUrl + UriConstant.ALI_REFUND_CALLBACK_V1);
                break;
        }
    }

}
