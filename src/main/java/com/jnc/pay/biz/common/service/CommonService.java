package com.jnc.pay.biz.common.service;

import com.jnc.pay.biz.common.mapper.RefundMapper;
import com.jnc.pay.biz.common.mapper.TradeMapper;
import com.jnc.pay.biz.common.model.Contract;
import com.jnc.pay.biz.common.model.Refund;
import com.jnc.pay.biz.common.model.Trade;
import com.jnc.pay.biz.common.model.Trans;
import com.jnc.pay.biz.common.vo.PayChangeReq;
import com.jnc.pay.biz.common.vo.PayReq;
import com.jnc.pay.biz.common.vo.RefundReq;
import com.jnc.pay.core.config.redis.lock.DistributedLock;

/**
 * @Auther: jjn
 * @Date: 2020/7/29
 * @Desc:
 */
public interface CommonService {

    /**
     * 分布式锁select + insert，实现支付接口幂等性
     * @param model
     * @param locker
     * @param lockKey
     * @param tradeMapper
     */
    public void addTrade(Trade model, DistributedLock locker, String lockKey, TradeMapper tradeMapper);

    /**
     * 分布式锁select + insert，实现退款接口幂等性
     * @param model
     * @param locker
     * @param lockKey
     * @param refundMapper
     */
    public void addRefund(Refund model, DistributedLock locker, String lockKey, RefundMapper refundMapper);

    /**
     * 幂等性新增支付数据
     * @param req
     */
    public void addTrade(PayReq req);

    /**
     * 幂等性新增退款数据
     * @param req
     */
    public void addRefund(RefundReq req);

    /**
     * 幂等性新增企业付款到零钱数据
     * @param model
     */
    public void addTrans(Trans model);

    /**
     * 幂等性新增签约数据
     * @param model
     */
    public void addContract(Contract model);
}
