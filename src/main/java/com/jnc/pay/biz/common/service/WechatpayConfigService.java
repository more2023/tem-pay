package com.jnc.pay.biz.common.service;

import com.jnc.pay.biz.common.model.WechatpayConfig;

import java.util.List;

/**
 * @Auther: jjn
 * @Date: 2020/5/11 19:19
 * @Desc:
 */
public interface WechatpayConfigService {

    /**
     * 获取有效的appId和wechatpay配置关联List
     * @return
     */
    public List<WechatpayConfig> queryWechatpayAppConfig();

    /**
     * 根据appId，获取wechatpay配置信息
     * @param appId
     * @return
     */
    public WechatpayConfig getWechatpayAppConfig(String appId);

    /**
     * 加载微信支付配置到redis
     */
    public void loadRedisWechatpayConfig();
}
