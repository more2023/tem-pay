package com.jnc.pay.biz.common.model;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 微信支付配置表 config_wechatpay
 * @author jjn
 * @date 2020-05-11 18:57:41
 */
@Setter
@Getter
public class WechatpayConfig {
	/** ID */
	private Long id;
	/** 微信应用ID */
	private String wechatpayAppId;
	/** 应用秘钥 */
	private String appSecret;
	/** 商户ID */
	private String merchantId;
	/** API秘钥 */
	private String apiSecret;
	/** 员工账号(用于退款及退款查询) */
	private String employeeId;
	/** API证书 */
	private String apiCertificate;
	/** API证书秘钥 */
	private String privateKey;
	/** 状态 0:正常, 1:禁用, -1:删除 */
	private Integer status;
	/** 创建时间 */
	private Integer createTime;
	/** 创建人id */
	private Integer createBy;
	/** 更新时间 */
	private Integer updateTime;
	/** 修改人id */
	private Integer updateBy;

	/** 应用ID */
	private String appId;

    public String toString() {
        return new ToStringBuilder(this)
            .append("id", getId())
            .append("wechatpayAppId", getWechatpayAppId())
            .append("appSecret", getAppSecret())
            .append("merchantId", getMerchantId())
            .append("apiSecret", getApiSecret())
            .append("employeeId", getEmployeeId())
            .append("apiCertificate", getApiCertificate())
            .append("privateKey", getPrivateKey())
            .append("status", getStatus())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
