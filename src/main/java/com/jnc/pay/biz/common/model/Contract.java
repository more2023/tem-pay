package com.jnc.pay.biz.common.model;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 签约流水表 pay_contract
 * @author jjn
 * @date 2020-08-04
 */
@Setter
@Getter
public class Contract {
	private static final long serialVersionUID = 1L;
	
	/** 签约唯一id，整个支付系统唯一 */
	private Long contractId;
	/** 应用id */
	private String appId;
	/** 支付渠道ID(如：alipay、wechatpay、unionpay) */
	private String channelId;
	/** 支付类型，比如：微信支付 */
	private String payType;
	/** 签约协议号 */
	private String contractCode;
	/** 签约展示名称 */
	private String contractName;
	/** 签约后，异步通知url */
	private String notifyUrl;
	/** 额外参数 */
	private String extra;
	/** 协议到期时间 */
	private String contractExpire;
	/** 协议解约方式(0:未解约、1:有效期过自动解约、2:用户主动解约、3:商户API解约、4:商户平台解约、5:注销) */
	private Integer terminationMode;
	/** 解约说明 */
	private String terminationDesc;
	/** 客户端ip */
	private String clientIp;
	/** 解约时间时间 */
	private Integer terminationTime;
	/** 第三方签约成功的时间 */
	private Integer transTime;
	/** 签约通知上游系统的时间 */
	private Integer finishTime;
	/** 第三方的签约代扣流水号 */
	private String contractNo;
	/** 0:待签约，1:签约成功， 2:签约业务处理完成，3:签约失败，5:解约成功，6:解约业务处理完成，7:解约失败 */
	private Integer contractStatus;
	/** 渠道签约错误码 */
	private String erorrCode;
	/** 渠道签约错误描述 */
	private String erorrMsg;
	/** 扩展参数1 */
	private String param1;
	/** 扩展参数2 */
	private String param2;
	/** 创建时间 */
	private Integer createTime;
	/** 更新时间 */
	private Integer updateTime;

	/** 原始状态 */
	private Integer oldContractStatus;

    public String toString() {
        return new ToStringBuilder(this)
            .append("contractId", getContractId())
            .append("appId", getAppId())
            .append("channelId", getChannelId())
            .append("payType", getPayType())
            .append("contractCode", getContractCode())
            .append("contractName", getContractName())
            .append("notifyUrl", getNotifyUrl())
            .append("extra", getExtra())
            .append("contractExpire", getContractExpire())
            .append("terminationMode", getTerminationMode())
            .append("terminationDesc", getTerminationDesc())
            .append("clientIp", getClientIp())
            .append("terminationTime", getTerminationTime())
            .append("transTime", getTransTime())
            .append("finishTime", getFinishTime())
            .append("contractNo", getContractNo())
            .append("contractStatus", getContractStatus())
            .append("erorrCode", getErorrCode())
            .append("erorrMsg", getErorrMsg())
            .append("param1", getParam1())
            .append("param2", getParam2())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
