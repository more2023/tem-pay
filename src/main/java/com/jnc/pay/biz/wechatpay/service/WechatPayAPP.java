package com.jnc.pay.biz.wechatpay.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.map.MapUtil;
import com.jnc.pay.biz.common.mapper.TradeMapper;
import com.jnc.pay.biz.common.model.Contract;
import com.jnc.pay.biz.common.model.Trade;
import com.jnc.pay.biz.common.service.CommonService;
import com.jnc.pay.biz.common.service.impl.AbstractWechatPay;
import com.jnc.pay.biz.common.vo.*;
import com.jnc.pay.constant.BizConstant;
import com.jnc.pay.core.config.redis.RedisStore;
import com.jnc.pay.core.exception.BusinessException;
import com.jnc.pay.core.model.BaseResp;
import com.jnc.pay.util.pay.WechatPayUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @Auther: jjn
 * @Date: 2020/6/30 10:30
 * @Desc: 微信APP支付（APP）
 */
@Slf4j
@Service
public class WechatPayAPP extends AbstractWechatPay {

    @Autowired
    RedisStore redisStore;
    @Autowired
    WechatPayUtil wechatPayUtil;
    @Resource
    TradeMapper tradeMapper;
    @Autowired
    CommonService commonService;

    @Override
    public BaseResp<Map<String, Object>> doPay(PayReq req) {
        Map<String, Object> map = MapUtil.newHashMap();
        //2、组装微信APP，支付请求参数
        String xmlReq = fillPayReq(map, req);

        //3、组装微信APP，支付响应参数
        return fillPayResp(xmlReq, req);
    }

    @Override
    public BaseResp<Map<String, Object>> doQuery(QueryReq req) {
        Map<String, Object> map = MapUtil.newHashMap();
        //2、组装微信APP，查询请求参数
        String xmlReq = fillQueryReq(map, req);

        //3、组装微信APP，查询响应参数
        return fillQueryResp(xmlReq, req);
    }

    @Override
    public BaseResp<Map<String, Object>> doRefund(RefundReq req) {
        Map<String, Object> map = MapUtil.newHashMap();
        //获取支付订单数据
        Trade trade = tradeMapper.getTradeById(req.getTradeId());
        if(trade == null){
            log.warn("[Wechatpay.APP] No valid refund orders, req: {}", req);
            return BaseResp.fail("no valid refund orders");
        }
        req.setTotalFee(trade.getTotalFee());
        req.setTradeNo(trade.getTradeNo());
        req.setAppOrderId(trade.getAppOrderId());
        if(req.getRefundFee().intValue() != trade.getTotalFee().intValue()){
            log.warn("[Wechatpay.APP] The order amount is inconsistent with the refund amount, req: {}", req);
            return BaseResp.fail("refund fee mismatch");
        }
        //2、组装微信APP，退款请求参数
        String xmlReq = fillRefundReq(map, req);

        //3、组装微信APP，退款响应参数
        return fillRefundResp(xmlReq, req);
    }

    @Override
    public BaseResp<Map<String, Object>> doRefundQuery(RefundQueryReq req) {
        Map<String, Object> map = MapUtil.newHashMap();
        //2、组装微信APP，查询请求参数
        String xmlReq = fillRefundQueryReq(map, req);

        //3、组装微信APP，查询响应参数
        return fillRefundQueryResp(xmlReq, req);
    }

    @Override
    protected void addTrade(PayReq req) {
        commonService.addTrade(req);
    }

    @Override
    protected void addRefund(RefundReq req) {
        commonService.addRefund(req);
    }

    @Override
    protected void addContract(ContractReq req) {
        Contract model = new Contract();
        BeanUtil.copyProperties(req, model);
        model.setContractStatus(BizConstant.WAIT_CONTRACT);
        commonService.addContract(model);
    }

    @Override
    public BaseResp<Map<String, Object>> doEnterPayChange(PayChangeReq req) {
        throw new BusinessException("Features not supported");
    }

    @Override
    public BaseResp<Map<String, Object>> doContract(ContractReq req) {
        Map<String, Object> map = MapUtil.newHashMap();
        //2、组装微信APP，查询请求参数
        String xmlReq = fillContractReq(map, req);

        //3、组装微信APP，查询响应参数
        return fillContractResp(xmlReq, req);
    }
}
