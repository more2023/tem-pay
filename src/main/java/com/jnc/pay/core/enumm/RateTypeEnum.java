package com.jnc.pay.core.enumm;

import lombok.Getter;

/**
 * @Auther: jjn
 * @Date: 2020/6/24 12:06
 * @Desc: 币种枚举类
 */
@Getter
public enum RateTypeEnum {
    CNY("CNY", "人民币");

    private String type;
    private String desc;

    RateTypeEnum(String type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    /**
     * 判断币种是否存在
     * @param type
     * @return
     */
    public static boolean exist(String type) {
        for (RateTypeEnum rateTypeEnum : RateTypeEnum.values()) {
            if (rateTypeEnum.getType().equals(type.trim())) {
                return true;
            }
        }
        return false;
    }
}
