package com.jnc.pay.core.vertx.verticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.DeliveryOptions;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author: jjn
 * @Date: 2019/2/1 18:23
 * @Desc: 多线程WorkerVerticle：这类Verticle也会运行在Worker Pool中的线程上。
 * 一个实例可以由多个线程同时执行
 */
public class MultiThreadedWorkerVerticle extends AbstractVerticle {

    @Autowired
    private DeliveryOptions deliveryOptions;

    public DeliveryOptions getDeliveryOptions(){
        return deliveryOptions;
    }

}
