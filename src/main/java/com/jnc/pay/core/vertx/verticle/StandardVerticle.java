package com.jnc.pay.core.vertx.verticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.DeliveryOptions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author: jjn
 * @Date: 2019/2/1 18:17
 * @Desc: 标准Verticle：这类Verticle永远运行在Event Loop线程上
 */
@Slf4j
public class StandardVerticle extends AbstractVerticle {

    @Autowired
    private DeliveryOptions deliveryOptions;

    public DeliveryOptions getDeliveryOptions(){
        return deliveryOptions;
    }
}
