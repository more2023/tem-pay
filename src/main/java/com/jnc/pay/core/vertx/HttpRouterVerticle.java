package com.jnc.pay.core.vertx;

import com.jnc.pay.core.vertx.verticle.WorkerVerticle;
import io.vertx.ext.web.Router;
import org.springframework.beans.factory.annotation.Autowired;

public class HttpRouterVerticle extends WorkerVerticle {

	@Autowired
    protected Router router;
	
	public Router getRouter() {
		return router;
	}
}
