package com.jnc.pay.core.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Auther: jjn
 * @Date: 2020/5/8 11:28
 * @Desc: http 客户端配置类
 */
@Setter
@Getter
@Component
@ConfigurationProperties(prefix = "http.client")
public class HttpClientPro {
    /**
     * http连接超时时间,单位毫秒ms
     */
    private int connectTimeout;
    /**
     * http连接空闲超时时间,单位秒s
     */
    private int idleTimeout;
    /**
     * 是否保持长连接
     */
    private boolean keepAlive = true;
    /**
     * 最大线程数
     */
    private int maxPoolSize;
    /**
     * 设置是否禁用Nagle算法，设置true后禁用Nagle算法，默认为false（即默认启用Nagle算法）
     */
    private boolean tcpNoDelay = true;
    /**
     * 最大请求等待队列长度，负数则代表无限制
     */
    private int maxWaitQueueSize;
    /**
     * 设置是否启用Netty池缓冲区,true启用
     */
    private boolean usePooledBuffers = true;
    /**
     * 设置是否启用管道，true启用
     */
    private boolean pipelining = true;

}
