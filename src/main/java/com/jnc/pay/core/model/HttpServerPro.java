package com.jnc.pay.core.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Auther: jjn
 * @Date: 2020/5/8 11:28
 * @Desc: http 服务端配置
 */
@Setter
@Getter
@Component
@ConfigurationProperties(prefix = "http.server")
public class HttpServerPro {
    /**
     * 服务ip (HttpServer默认主机名是0.0.0.0，它表示：监听所有可用地址；默认端口号是80)
     */
    private String host;
    /**
     * 设置接收buffer的size
     */
    private int receiveBufferSize;
    /**
     * 设置TCP是否保持激活  true:激活
     */
    private boolean tcpKeepAlive = true;
    /**
     * 设置是否启用TCP不延迟  true:不延迟
     */
    private boolean tcpNoDelay = true;
    /**
     * 设置是否继续自动处理100
     */
    private boolean handle100ContinueAutomatically = true;
    /**
     * 设置最大的HTTP块大小
     */
    private int maxChunkSize;

}
