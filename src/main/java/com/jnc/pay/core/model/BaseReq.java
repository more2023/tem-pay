package com.jnc.pay.core.model;

import com.jnc.pay.biz.common.model.AlipayConfig;
import com.jnc.pay.biz.common.model.WechatpayConfig;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: jjn
 * @Date: 2018/11/14 12:31
 * @Desc: 基础 req
 */
public class BaseReq implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 应用ID */
    @NotBlank(message = "应用ID不能为空")
    @Length(min = 1, max = 32, message = "应用ID长度不能超过32位")
    private String appId;
    /** 渠道ID（alipay、wechatpay、unionpay） */
    @NotBlank(message = "渠道ID不能为空")
    @Length(min = 1, max = 32, message = "渠道ID长度不能超过32位")
    private String channelId;
    /** 支付类型 */
    @NotBlank(message = "支付类型不能为空")
    @Length(min = 1, max = 32, message = "支付类型长度不能超过32位")
    private String payType;

    /** ========================== 程序自定义字段 ============================ */
    //分发路径
    private String path;
    //微信支付配置
    private WechatpayConfig wechatpayConfig;
    //支付宝支付配置
    private AlipayConfig alipayConfig;

    /** 请求参数 */
    private Map<String, Object> params = new HashMap<>();

    public Object getParams(String key) {
        return params.get(key);
    }

    public void setParams(String key, Object obj) {
        params.put(key, obj);
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public WechatpayConfig getWechatpayConfig() {
        return wechatpayConfig;
    }

    public void setWechatpayConfig(WechatpayConfig wechatpayConfig) {
        this.wechatpayConfig = wechatpayConfig;
    }

    public AlipayConfig getAlipayConfig() {
        return alipayConfig;
    }

    public void setAlipayConfig(AlipayConfig alipayConfig) {
        this.alipayConfig = alipayConfig;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
