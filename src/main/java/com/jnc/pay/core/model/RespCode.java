package com.jnc.pay.core.model;

/**
 * @Auther: jjn
 * @Date: 2019/12/24 11:51
 * @Desc: 响应状态枚举类
 */
public enum RespCode {

    SUCCESS(0,"SUCCESS"),
    ERROR(1,"ERROR"),
    ILLEGAL_ARGUMENT(2,"Illegal argument"),
    NOT_WHITE_IP(3, "Not white list ip"),
    INVALID_MCH_ID(4, "Invalid mch id"),
    TIME_STAMP_EXPIRE(5, "Timestamp expired"),
    SIGN_VERIFY_FAIL(6, "sign verify failed"),

    WX_BIZ_ERROR(51, "WX_BIZ_ERROR"),
    WX_SYS_ERROR(52, "WX_SYS_ERROR"),
    ALI_BIZ_ERROR(71, "ALI_BIZ_ERROR"),
    ALI_SYS_ERROR(72, "ALI_SYS_ERROR");
    
    private final int code;
    private final String desc;

    RespCode(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode(){
        return code;
    }

    public String getDesc(){
        return desc;
    }
}
