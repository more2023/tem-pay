package com.jnc.pay.core.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Auther: jjn
 * @Date: 2020/5/8 11:18
 * @Desc: vertx对象初始化配置类
 */
@Setter
@Getter
@Component
@ConfigurationProperties(prefix = "vertx")
public class VertxPro {
    /**
     * 阻塞线程检查的时间间隔，默认1000，单位ms，即1秒
      */
    private long blockedThreadCheckInterval;
    /**
     * Event Loop的最大执行时间，默认2L * 1000 * 1000000，单位ns，即2秒
     */
    private long maxEventLoopExecuteTime;
    /**
     * Worker线程的最大执行时间，默认60l * 1000 * 1000000，单位ns，即60秒
     */
    private long maxWorkerExecuteTime;
    /**
     * 设置Vert.x实例中支持的Worker线程的最大数量，默认值为20
     */
    private int workerPoolSize;
    /**
     * 设置Vert.x实例中使用的Event Loop线程的数量，默认值为：2的n次方
     */
    private int eventLoopPoolSize;
    /**
     * 如果线程阻塞时间超过了这个阀值，那么就会打印警告的堆栈信息，默认为5l * 1000 * 1000000，单位ns，即5秒
     */
    private long warningExceptionTime;
}
