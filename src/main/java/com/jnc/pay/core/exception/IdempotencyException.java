package com.jnc.pay.core.exception;

/**
 * @Auther: jjn
 * @Date: 2020/7/22
 * @Desc: 幂等性异常
 */
public class IdempotencyException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public IdempotencyException(String message) {
        super(message);
    }
}
