package com.jnc.pay.core.exception;

/**
 * @Auther: jjn
 * @Date: 2020/7/22
 * @Desc: 分布式锁异常
 */
public class LockException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public LockException(String message) {
        super(message);
    }
}
