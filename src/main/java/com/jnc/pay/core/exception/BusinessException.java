package com.jnc.pay.core.exception;

/**
 * @Auther: jjn
 * @Date: 2020/7/22
 * @Desc: 业务异常
 */
public class BusinessException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public BusinessException(String message) {
        super(message);
    }
}
