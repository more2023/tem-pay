package com.jnc.pay.util.pay;

import lombok.extern.slf4j.Slf4j;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @Auther: jjn
 * @Date: 2020/6/28 12:44
 * @Desc: 支付sign工具类(MD5)
 */
@Slf4j
@SuppressWarnings("unchecked")
public class SignUtil {
    private static final String encodingCharset = "UTF-8";

    /**
     * byte[]转16进制字符串
     * @param input
     * @return
     */
    public static String toHex(byte input[]) {
        if (input == null)
            return null;
        StringBuffer output = new StringBuffer(input.length * 2);
        for (int i = 0; i < input.length; i++) {
            int current = input[i] & 0xff;
            if (current < 16)
                output.append("0");
            output.append(Integer.toString(current, 16));
        }

        return output.toString();
    }

    /**
     * MD5加密
     * @param value    加密字符串
     * @param charset  加密字符集
     * @return
     */
    public static String md5(String value, String charset) {
        MessageDigest md = null;
        try {
            byte[] data = value.getBytes(charset);
            md = MessageDigest.getInstance("MD5");
            byte[] digestData = md.digest(data);
            return toHex(digestData);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 获取MD5加密算法，加密后的sign（去掉空属性值）
     * @param o   加密对象
     * @param key 加密盐值
     * @return
     * @throws IllegalAccessException
     */
    public static String getSign(Object o, String key) throws IllegalAccessException {
        if(o instanceof Map) {
            return getSign((Map<String, Object>)o, key);
        }
        ArrayList<String> list = new ArrayList<String>();
        Class cls = o.getClass();
        Field[] fields = cls.getDeclaredFields();
        for (Field f : fields) {
            f.setAccessible(true);
            if (f.get(o) != null && !"".equals(f.get(o))) {
                list.add(f.getName() + "=" + f.get(o) + "&");
            }
        }
        int size = list.size();
        String [] arrayToSort = list.toArray(new String[size]);
        Arrays.sort(arrayToSort, String.CASE_INSENSITIVE_ORDER);
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < size; i ++) {
            sb.append(arrayToSort[i]);
        }
        String result = sb.toString();
        result += "key=" + key;
        log.debug("Sign Before MD5:" + result);
        result = md5(result, encodingCharset).toUpperCase();
        log.debug("Sign Result:" + result);
        return result;
    }

    /**
     * 获取MD5加密算法，加密后的sign（去掉空属性值）
     * @param map 加密map
     * @param key 加密盐值
     * @return
     */
    public static String getSign(Map<String,Object> map, String key){
        ArrayList<String> list = new ArrayList<String>();
        for(Map.Entry<String,Object> entry:map.entrySet()){
            if(!"".equals(entry.getValue()) && null != entry.getValue()){
                list.add(entry.getKey() + "=" + entry.getValue() + "&");
            }
        }
        int size = list.size();
        String [] arrayToSort = list.toArray(new String[size]);
        Arrays.sort(arrayToSort, String.CASE_INSENSITIVE_ORDER);
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < size; i ++) {
            sb.append(arrayToSort[i]);
        }
        String result = sb.toString();
        result += "key=" + key;
        log.debug("Sign Before MD5:" + result);
        result = md5(result, encodingCharset).toUpperCase();
        log.debug("Sign Result:" + result);
        return result;
    }

    /**
     * 获取MD5加密算法，加密后的sign（去掉空属性值）
     * @param map 加密map
     * @param key 加密盐值
     * @param notContains 不包含的签名字段
     * @return
     */
    public static String getSign(Map<String,Object> map, String key, String... notContains){
        Map<String,Object> newMap = new HashMap<String,Object>();
        for(Map.Entry<String,Object> entry:map.entrySet()){
            boolean isContain = false;
            for(int i=0; i<notContains.length; i++) {
                if(entry.getKey().equals(notContains[i])) {
                    isContain = true;
                    break;
                }
            }
            if(!isContain) {
                newMap.put(entry.getKey(), entry.getValue());
            }
        }
        return getSign(newMap, key);
    }
}
