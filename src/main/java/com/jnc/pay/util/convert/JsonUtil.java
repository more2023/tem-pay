package com.jnc.pay.util.convert;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.TypeReference;
import com.jnc.pay.biz.common.model.DictData;
import lombok.extern.slf4j.Slf4j;

import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * @Auther: jjn
 * @Date: 2019/12/9 12:04
 * @Desc: Json工具类
 */
@Slf4j
@SuppressWarnings({ "rawtypes", "unchecked" })
public class JsonUtil {

    private static final String CHARSET_NAME = "UTF-8";

    /**
     * json字符串转换成对象
     * @param jsonString
     * @param clazz
     * @return
     */
    public static <T> T json2Bean(String jsonString, Class<T> clazz){
        T t = null;
        try {
            t = JSON.parseObject(jsonString, clazz);
        } catch (Exception e) {
            log.error("JsonUtil.json2Bean Exception: {}", e);
        }
        return t;
    }

    /**
     * json字符串转换成泛型对象
     * @param jsonString
     * @param type
     * @param <T>
     * @return
     */
    public static <T> T json2Bean(String jsonString, TypeReference<T> type){
        T t = null;
        try {
            t = JSON.parseObject(jsonString, type);
        } catch (Exception e) {
            log.error("JsonUtil.json2Bean Exception: {}", e);
        }
        return t;
    }

    /**
     * 对象转换成json字符串
     * @param obj
     * @return
     */
    public static String bean2Json(Object obj){
        return JSON.toJSONString(obj);
    }

    /**
     * 对象转json Byte数组
     * @param obj
     * @return
     */
    public static byte[] bean2JsonBytes(Object obj) {
        //把对象转换成JSON
        String json = bean2Json(obj);
        try {
            return json.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            log.error("JsonUtil.bean2JsonBytes Exception: {}", e);
        }
        return null;
    }

    /**
     * json字节数组转对象
     * @param bytes
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T jsonBytes2Bean(byte[] bytes, Class<T> clazz){
        String json = ConvertUtil.str(bytes, CHARSET_NAME);
        return json2Bean(json, clazz);
    }

    /**
     * json字符串转换成List集合
     * @param jsonString
     * @return
     */
   
	public static <T> List<T> json2List(String jsonString, Class clazz){
        List<T> list = null;
        try {
            list = JSON.parseArray(jsonString, clazz);
        } catch (Exception e) {
            log.error("JsonUtil.json2List Exception: {}", e);
        }
        return list;
    }

    /**
     * List集合转换成json字符串
     * @param obj
     * @return
     */
    public static String list2Json(Object obj){
        return JSONArray.toJSONString(obj, true);
    }

    /**
     * json转List
     * (不需要实体类)
     * @param jsonStr
     * @return
     */
    public static JSONArray json2List(String jsonStr){
        return JSON.parseArray(jsonStr);
    }
}
