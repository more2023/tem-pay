package com.jnc.pay.util.convert;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.map.MapUtil;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.*;

/**
 * @Auther: jjn
 * @Date: 2020/5/12 18:56
 * @Desc:
 */
public class CustomBeanUtil {

    /**
     * obj转Map
     * @param obj
     * @return
     */
    public static Map<String, Object> objToMap(Object obj){
        //isToUnderlineCase：是否转换为下划线模式
        //ignoreNullValue：是否忽略值为空的字段
        return BeanUtil.beanToMap(obj, false, true);
    }

    /**
     * map转obj
     * @param map
     * @param clazz
     * @return
     */
    public static Object mapToObj(Map<String, Object> map, Class<?> clazz){
        //isIgnoreError：是否忽略注入错误
        return BeanUtil.mapToBean(map, clazz, true);
    }

    /**
     * 去除map的空值，得到一个新的map
     * @param map
     * @return
     */
    public static Map<String, Object> mapFilter(Map<String, Object> map){
        Map<String, Object> respMap = MapUtil.newHashMap();
        if(map == null || map.size() < 1){
            return respMap;
        }
        for (String key : map.keySet()) {
            Object value = map.get(key);
            if (value == null || "".equals(value)) {
                continue;
            }
            respMap.put(key, value);
        }
        return respMap;
    }

    /**
     * 参数按照字段名的 ASCII 码从小到大排序（字典序），并按照"参数key=参数value"的模式用 & 拼接成字符串
     * @param map 需要排序并拼接字符串的map
     * @return
     */
    public static String LinkStr(Map<String, Object> map){
        List<String> keys = new ArrayList<>(map.keySet());
        Collections.sort(keys);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < keys.size(); i++) {
            String key = keys.get(i);
            Object value = map.get(key);
            //拼接时，不包括最后一个&字符
            if (i == keys.size() - 1) {
                sb.append(key).append("=").append(value);
            } else {
                sb.append(key).append("=").append(value).append("&");
            }
        }
        return sb.toString();
    }

    /**
     * map转url拼接参数字符串
     * @param map
     * @return
     */
    public static String mapToParams(Map<String,Object> map){
        StringBuilder sb = new StringBuilder();
        if(map == null || map.isEmpty()){
            return "" ;
        }

        for(String key: map.keySet()){
            String value = (String)map.get(key);
            if(sb.length() < 1){
                sb.append(key).append("=").append(value);
            }else{
                sb.append("&").append(key).append("=").append(value);
            }
        }
        return sb.toString();
    }

    /**
     * 生成随机数
     * @param length  生成字符串的长度
     * @return
     */
    public static String getRandomStr(int length) {
        String base = "abcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }
}
