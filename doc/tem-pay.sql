-- ----------------------------
-- 1、用户表
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户编号',
  `login_name` varchar(30) NOT NULL COMMENT '登录账号',
  `user_name` varchar(255) DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) NOT NULL COMMENT '密码',
  `sex` varchar(6) DEFAULT NULL COMMENT '性别',
  `birthday` datetime DEFAULT NULL COMMENT '出生日期',
  `phone` varchar(255) DEFAULT NULL COMMENT '电话号码',
  `address` varchar(255) DEFAULT NULL COMMENT '地址',
  `email` varchar(255) DEFAULT NULL COMMENT 'email',
  `status` varchar(2) NOT NULL DEFAULT '0' COMMENT '状态(0:正常;1:已锁定)',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `login_name` (`login_name`)
) ENGINE=InnoDB CHARSET=utf8mb4 COMMENT='用户信息表';

-- 账户：admin   -- 密码：123
INSERT INTO `sys_user` VALUES ('1', 'admin', '管理员', '88212f91e2e9cf36981a91b6c518af5c', '0', '2018-11-05 00:00:00', '12345678987', '湖南省长沙市', '8878@qq.com', '0', 'ghgjhhgjghfg', '2018-11-15 16:38:25', '2018-11-15 16:38:25');

-- ----------------------------
-- 2、角色表
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色编号',
  `role_code` varchar(30) NOT NULL COMMENT '角色编码',
  `role_name` varchar(50) NOT NULL COMMENT '角色名称',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `role_code` (`role_code`)
) ENGINE=InnoDB CHARSET=utf8mb4 COMMENT='角色信息表';

INSERT INTO `sys_role` VALUES ('1', 'admin', '管理员', null, null, '2018-11-08 17:20:55');

-- ----------------------------
-- 3、用户角色关联表
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户和角色关联表';

INSERT INTO `sys_user_role` VALUES ('1', '1');

-- ----------------------------
-- 4、菜单表
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '菜单编号',
  `parent_id` int(11) DEFAULT NULL COMMENT '上一级菜单id',
  `menu_type` varchar(2) DEFAULT NULL COMMENT '菜单类型(1:目录;2:菜单;3:按钮)',
  `menu_name` varchar(255) DEFAULT NULL COMMENT '菜单名称',
  `permission` varchar(255) DEFAULT NULL COMMENT '权限标识(menu例子：role:*，button例子：role:create,role:update)',
  `menu_url` varchar(255) DEFAULT NULL COMMENT '菜单url',
  `menu_icon` varchar(255) DEFAULT NULL COMMENT '菜单图标url',
  `is_hide` varchar(2) DEFAULT '0' COMMENT '是否隐藏(0:显示;1:隐藏)',
  `menu_sort` int(11) DEFAULT '0' COMMENT '排序序号',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB CHARSET=utf8mb4 COMMENT='菜单权限表';

INSERT INTO `sys_menu` VALUES ('1', '0', '1', '系统管理', '', '', 'fa fa-bank', '0', '1', '', '2018-11-15 15:37:55', '2018-11-15 15:37:55');
INSERT INTO `sys_menu` VALUES ('2', '0', '1', '系统工具', '', '', 'fa fa-anchor', '0', '2', '', '2018-11-15 15:38:24', '2018-11-15 15:38:24');
INSERT INTO `sys_menu` VALUES ('3', '1', '2', '用户管理', 'system:user:view', '/system/user', '', '0', '1', null, null, '2018-11-09 16:41:46');
INSERT INTO `sys_menu` VALUES ('4', '3', '3', '用户查询', 'system:user:list', '', '', '0', '1', '', null, '2018-11-13 16:18:38');
INSERT INTO `sys_menu` VALUES ('5', '3', '3', '用户新增', 'system:user:add', '', '', '0', '2', null, null, '2018-11-12 10:49:28');
INSERT INTO `sys_menu` VALUES ('6', '3', '3', '用户修改', 'system:user:edit', '', '', '0', '3', null, null, '2018-11-12 10:49:24');
INSERT INTO `sys_menu` VALUES ('7', '3', '3', '用户删除', 'system:user:remove', '', '', '0', '4', null, null, '2018-11-12 10:49:21');
INSERT INTO `sys_menu` VALUES ('8', '1', '2', '角色管理', 'system:role:view', '/system/role', '', '0', '1', null, null, '2018-11-11 21:46:44');
INSERT INTO `sys_menu` VALUES ('9', '8', '3', '角色查询', 'system:role:list', '', '', '0', '1', '', null, '2018-11-13 16:18:38');
INSERT INTO `sys_menu` VALUES ('10', '8', '3', '角色新增', 'system:role:add', '', '', '0', '2', null, null, '2018-11-12 10:49:06');
INSERT INTO `sys_menu` VALUES ('11', '8', '3', '角色修改', 'system:role:edit', '', '', '0', '3', null, null, '2018-11-12 10:49:09');
INSERT INTO `sys_menu` VALUES ('12', '8', '3', '角色删除', 'system:role:remove', '', '', '0', '4', null, null, '2018-11-12 10:49:12');
INSERT INTO `sys_menu` VALUES ('13', '1', '2', '菜单管理', 'system:menu:view', '/system/menu', '', '0', '3', null, null, '2018-11-12 10:47:43');
INSERT INTO `sys_menu` VALUES ('14', '13', '3', '菜单查询', 'system:menu:list', '', '', '0', '1', '', null, '2018-11-13 16:18:39');
INSERT INTO `sys_menu` VALUES ('15', '13', '3', '菜单新增', 'system:menu:add', '', '', '0', '2', null, null, '2018-11-12 10:48:58');
INSERT INTO `sys_menu` VALUES ('16', '13', '3', '菜单修改', 'system:menu:edit', '', '', '0', '3', null, null, '2018-11-12 12:33:49');
INSERT INTO `sys_menu` VALUES ('17', '13', '3', '菜单删除', 'system:menu:remove', '', '', '0', '4', null, null, '2018-11-12 12:34:23');
INSERT INTO `sys_menu` VALUES ('18', '1', '2', '字典管理', 'system:dict:view', '/system/dictType', '', '0', '4', '', '2018-11-13 16:11:38', '2018-11-13 16:12:54');
INSERT INTO `sys_menu` VALUES ('19', '18', '3', '字典查询', 'system:dict:list', '', '', '0', '1', '', null, '2018-11-13 16:18:42');
INSERT INTO `sys_menu` VALUES ('20', '18', '3', '字典新增', 'system:dict:add', '', '', '0', '2', '', '2018-11-13 16:11:13', '2018-11-13 16:12:56');
INSERT INTO `sys_menu` VALUES ('21', '18', '3', '字典修改', 'system:dict:edit', '', '', '0', '3', '', '2018-11-13 16:11:55', '2018-11-13 16:13:05');
INSERT INTO `sys_menu` VALUES ('22', '18', '3', '字典删除', 'system:dict:remove', '', '', '0', '4', '', '2018-11-13 16:12:25', '2018-11-13 16:13:10');
INSERT INTO `sys_menu` VALUES ('23', '2', '2', '代码生成', 'tool:gen:view', '/tool/gen', '', '0', '1', '', null, '2018-11-14 17:28:51');
INSERT INTO `sys_menu` VALUES ('24', '23', '3', '代码列表查询', 'tool:gen:list', '', '', '0', '1', '', null, '2018-11-14 17:29:43');
INSERT INTO `sys_menu` VALUES ('25', '23', '3', '生成代码', 'tool:gen:code', '', '', '0', '2', '', null, '2018-11-14 17:29:43');

-- ----------------------------
-- 5、角色菜单表
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `menu_id` int(11) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色和菜单关联表';

INSERT INTO `sys_role_menu` VALUES ('1', '1');
INSERT INTO `sys_role_menu` VALUES ('1', '2');
INSERT INTO `sys_role_menu` VALUES ('1', '3');
INSERT INTO `sys_role_menu` VALUES ('1', '4');
INSERT INTO `sys_role_menu` VALUES ('1', '5');
INSERT INTO `sys_role_menu` VALUES ('1', '6');
INSERT INTO `sys_role_menu` VALUES ('1', '7');
INSERT INTO `sys_role_menu` VALUES ('1', '8');
INSERT INTO `sys_role_menu` VALUES ('1', '9');
INSERT INTO `sys_role_menu` VALUES ('1', '10');
INSERT INTO `sys_role_menu` VALUES ('1', '11');
INSERT INTO `sys_role_menu` VALUES ('1', '12');
INSERT INTO `sys_role_menu` VALUES ('1', '13');
INSERT INTO `sys_role_menu` VALUES ('1', '14');
INSERT INTO `sys_role_menu` VALUES ('1', '15');
INSERT INTO `sys_role_menu` VALUES ('1', '16');
INSERT INTO `sys_role_menu` VALUES ('1', '17');
INSERT INTO `sys_role_menu` VALUES ('1', '18');
INSERT INTO `sys_role_menu` VALUES ('1', '19');
INSERT INTO `sys_role_menu` VALUES ('1', '20');
INSERT INTO `sys_role_menu` VALUES ('1', '21');
INSERT INTO `sys_role_menu` VALUES ('1', '22');
INSERT INTO `sys_role_menu` VALUES ('1', '23');
INSERT INTO `sys_role_menu` VALUES ('1', '24');
INSERT INTO `sys_role_menu` VALUES ('1', '25');

-- ----------------------------
-- 6、字典类型表
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type` (
  `dict_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `status` varchar(2) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`dict_id`),
  UNIQUE KEY `dict_type` (`dict_type`)
) ENGINE=InnoDB CHARSET=utf8mb4 COMMENT='字典类型表';

INSERT INTO `sys_dict_type` VALUES ('1', '界面皮肤样式', 'skin_style', '0', '2018-11-08 10:09:26', '');
INSERT INTO `sys_dict_type` VALUES ('2', '常用状态', 'normal_status', '0', '2018-11-13 17:02:57', '');
INSERT INTO `sys_dict_type` VALUES ('3', '系统默认', 'yes_no', '0', '2018-11-13 16:53:32', '');
INSERT INTO `sys_dict_type` VALUES ('4', '性别', 'sex_type', '0', '2018-11-13 17:13:56', '');
INSERT INTO `sys_dict_type` VALUES ('5', '是否可见', 'show_hide', '0', '2018-11-13 17:17:15', '');
INSERT INTO `sys_dict_type` VALUES ('6', '支付类型跟事件匹配配置', 'pay_type_event', '0', '2020-06-28 18:19:45', '');
INSERT INTO `sys_dict_type` VALUES ('7', '查询类型跟事件匹配配置', 'query_type_event', '0', '2020-06-30 17:08:05', '');
INSERT INTO `sys_dict_type` VALUES ('8', '退款类型跟事件匹配配置', 'refund_type_event', '0', '2020-07-01 09:34:14', '');
INSERT INTO `sys_dict_type` VALUES ('9', '退款查询类型跟事件匹配配置', 'refund_query_type_event', '0', '2020-07-07 11:04:14', '');
INSERT INTO `sys_dict_type` VALUES ('10', '转账类型跟事件匹配配置', 'trans_type_event', '0', '2020-08-03 16:33:35', '');
INSERT INTO `sys_dict_type` VALUES ('11', '签约类型跟事件匹配配置', 'contract_type_event', '0', '2020-08-04 13:42:26', '');
INSERT INTO `sys_dict_type` VALUES ('12', '签约查询类型事件匹配配置', 'contract_query_type_event', '0', '2020-08-05 10:07:26', '');
INSERT INTO `sys_dict_type` VALUES ('13', '代扣类型跟事件匹配配置', 'withhold_type_event', '0', '2020-08-05 11:05:20', '');
INSERT INTO `sys_dict_type` VALUES ('14', 'ip白名单配置', 'white_list_ip', '0', '2020-07-24 16:50:57', '');

-- ----------------------------
-- 7、字典数据表
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data` (
  `data_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '字典数据主键',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `dict_code` varchar(100) DEFAULT '' COMMENT '字典编码',
  `dict_value` varchar(100) DEFAULT '' COMMENT '字典值',
  `dict_sort` int(4) DEFAULT '0' COMMENT '字典排序',
  `css_class` varchar(500) DEFAULT '' COMMENT '样式属性',
  `is_default` varchar(2) DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` varchar(2) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`data_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COMMENT='字典数据表';

INSERT INTO `sys_dict_data` VALUES ('1', 'skin_style', '1', 'skin-default', '0', 'info', 'Y', '0', '2018-11-08 10:17:11', '默认');
INSERT INTO `sys_dict_data` VALUES ('2', 'normal_status', '0', '正常', '1', 'info', 'Y', '0', '2018-11-13 16:46:44', '');
INSERT INTO `sys_dict_data` VALUES ('3', 'normal_status', '1', '停用', '2', 'danger', 'N', '0', '2018-11-13 16:46:30', '');
INSERT INTO `sys_dict_data` VALUES ('4', 'yes_no', 'Y', '是', '1', 'info', 'Y', '0', '2018-11-13 17:04:18', '');
INSERT INTO `sys_dict_data` VALUES ('5', 'yes_no', 'N', '否', '2', 'danger', 'N', '0', '2018-11-13 17:04:27', '');
INSERT INTO `sys_dict_data` VALUES ('6', 'sex_type', '0', '男', '1', 'info', 'Y', '0', '2018-11-15 11:30:25', '');
INSERT INTO `sys_dict_data` VALUES ('7', 'sex_type', '1', '女', '2', 'warning', 'N', '0', '2018-11-15 11:32:16', '');
INSERT INTO `sys_dict_data` VALUES ('8', 'show_hide', '0', '显示', '1', 'info', 'Y', '0', '2018-11-13 17:18:11', '');
INSERT INTO `sys_dict_data` VALUES ('9', 'show_hide', '1', '隐藏', '2', 'danger', 'N', '0', '2018-11-13 17:18:45', '');
INSERT INTO `sys_dict_data` VALUES ('10', 'pay_type_event', 'NATIVE', 'event.wechat.pay.native', '0', '', 'N', '0', '2020-06-30 17:01:42', '');
INSERT INTO `sys_dict_data` VALUES ('11', 'pay_type_event', 'JSAPI', 'event.wechat.pay.jsapi', '0', '', 'N', '0', '2020-06-30 17:01:48', '');
INSERT INTO `sys_dict_data` VALUES ('12', 'pay_type_event', 'MWEB', 'event.wechat.pay.h5', '0', '', 'N', '0', '2020-06-30 17:01:53', '');
INSERT INTO `sys_dict_data` VALUES ('13', 'pay_type_event', 'APP', 'event.wechat.pay.app', '0', '', 'N', '0', '2020-06-30 17:01:59', '');
INSERT INTO `sys_dict_data` VALUES ('14', 'query_type_event', 'NATIVE', 'event.wechat.query.native', '0', '', 'N', '0', '2020-06-30 17:08:52', '');
INSERT INTO `sys_dict_data` VALUES ('15', 'query_type_event', 'JSAPI', 'event.wechat.query.jsapi', '0', '', 'N', '0', '2020-06-30 17:09:38', '');
INSERT INTO `sys_dict_data` VALUES ('16', 'query_type_event', 'MWEB', 'event.wechat.query.h5', '0', '', 'N', '0', '2020-06-30 17:10:12', '');
INSERT INTO `sys_dict_data` VALUES ('17', 'query_type_event', 'APP', 'event.wechat.query.app', '0', '', 'N', '0', '2020-06-30 17:10:32', '');
INSERT INTO `sys_dict_data` VALUES ('18', 'refund_type_event', 'NATIVE', 'event.wechat.refund.native', '0', '', 'N', '0', '2020-07-01 09:34:57', '');
INSERT INTO `sys_dict_data` VALUES ('19', 'refund_type_event', 'JSAPI', 'event.wechat.refund.jsapi', '0', '', 'N', '0', '2020-07-01 09:35:23', '');
INSERT INTO `sys_dict_data` VALUES ('20', 'refund_type_event', 'MWEB', 'event.wechat.refund.h5', '0', '', 'N', '0', '2020-07-01 09:35:43', '');
INSERT INTO `sys_dict_data` VALUES ('21', 'refund_type_event', 'APP', 'event.wechat.refund.app', '0', '', 'N', '0', '2020-07-01 09:36:01', '');
INSERT INTO `sys_dict_data` VALUES ('22', 'refund_query_type_event', 'NATIVE', 'event.wechat.refund.query.native', '0', '', 'N', '0', '2020-07-07 11:04:52', '');
INSERT INTO `sys_dict_data` VALUES ('23', 'refund_query_type_event', 'JSAPI', 'event.wechat.refund.query.jsapi', '0', '', 'N', '0', '2020-07-07 11:05:22', '');
INSERT INTO `sys_dict_data` VALUES ('24', 'refund_query_type_event', 'MWEB', 'event.wechat.refund.query.h5', '0', '', 'N', '0', '2020-07-07 11:07:52', '');
INSERT INTO `sys_dict_data` VALUES ('25', 'refund_query_type_event', 'APP', 'event.wechat.refund.query.app', '0', '', 'N', '0', '2020-07-07 11:08:10', '');
INSERT INTO `sys_dict_data` VALUES ('26', 'pay_type_event', 'PAGE', 'event.ali.pay.pcweb', '0', '', 'N', '0', '2020-07-08 14:05:13', '');
INSERT INTO `sys_dict_data` VALUES ('27', 'pay_type_event', 'WAP', 'event.ali.pay.mobileweb', '0', '', 'N', '0', '2020-07-08 14:05:54', '');
INSERT INTO `sys_dict_data` VALUES ('28', 'pay_type_event', 'ALIAPP', 'event.ali.pay.app', '0', '', 'N', '0', '2020-07-08 11:17:43', '');
INSERT INTO `sys_dict_data` VALUES ('29', 'query_type_event', 'PAGE', 'event.ali.query.pcweb', '0', '', 'N', '0', '2020-07-09 09:59:58', '');
INSERT INTO `sys_dict_data` VALUES ('30', 'query_type_event', 'WAP', 'event.ali.query.mobileweb', '0', '', 'N', '0', '2020-07-09 10:00:19', '');
INSERT INTO `sys_dict_data` VALUES ('31', 'query_type_event', 'ALIAPP', 'event.ali.query.app', '0', '', 'N', '0', '2020-07-09 10:00:43', '');
INSERT INTO `sys_dict_data` VALUES ('32', 'refund_type_event', 'PAGE', 'event.ali.refund.pcweb', '0', '', 'N', '0', '2020-07-09 14:04:43', '');
INSERT INTO `sys_dict_data` VALUES ('33', 'refund_type_event', 'WAP', 'event.ali.refund.mobileweb', '0', '', 'N', '0', '2020-07-09 14:05:11', '');
INSERT INTO `sys_dict_data` VALUES ('34', 'refund_type_event', 'ALIAPP', 'event.ali.refund.app', '0', '', 'N', '0', '2020-07-09 14:05:30', '');
INSERT INTO `sys_dict_data` VALUES ('35', 'refund_query_type_event', 'PAGE', 'event.ali.refund.query.pcweb', '0', '', 'N', '0', '2020-07-09 14:10:15', '');
INSERT INTO `sys_dict_data` VALUES ('36', 'refund_query_type_event', 'WAP', 'event.ali.refund.query.mobileweb', '0', '', 'N', '0', '2020-07-09 14:10:37', '');
INSERT INTO `sys_dict_data` VALUES ('37', 'refund_query_type_event', 'ALIAPP', 'event.ali.refund.query.app', '0', '', 'N', '0', '2020-07-09 14:10:56', '');
INSERT INTO `sys_dict_data` VALUES ('38', 'trans_type_event', 'SEP', 'event.wechat.enter.pay.change', '0', '', 'N', '0', '2020-08-05 11:06:46', '');
INSERT INTO `sys_dict_data` VALUES ('39', 'trans_type_event', 'OEP', 'event.wechat.enter.pay.change', '0', '', 'N', '0', '2020-08-05 11:06:42', '');
INSERT INTO `sys_dict_data` VALUES ('40', 'contract_type_event', 'JSAPI_CW', 'event.wechat.contract.jsapi', '0', '', 'N', '0', '2020-08-05 11:06:38', '');
INSERT INTO `sys_dict_data` VALUES ('41', 'contract_type_event', 'APP_CW', 'event.wechat.contract.app', '0', '', 'N', '0', '2020-08-05 11:06:34', '');
INSERT INTO `sys_dict_data` VALUES ('42', 'contract_query_type_event', 'JSAPI_CW', 'event.wechat.contract.query', '0', '', 'N', '0', '2020-08-05 11:06:31', '');
INSERT INTO `sys_dict_data` VALUES ('43', 'contract_query_type_event', 'APP_CW', 'event.wechat.contract.query', '0', '', 'N', '0', '2020-08-05 11:06:28', '');
INSERT INTO `sys_dict_data` VALUES ('44', 'withhold_type_event', 'PAP', 'event.wechat.withhold', '0', '', 'N', '0', '2020-08-05 11:06:23', '');
INSERT INTO `sys_dict_data` VALUES ('45', 'white_list_ip', '196.128.1.12', '测试客户端ip', '0', '', 'N', '0', '2020-07-29 15:45:33', '');

-- ----------------------------
-- 8、商户表
-- ----------------------------
DROP TABLE IF EXISTS `pay_mch`;
CREATE TABLE `pay_mch` (
  `mch_id` bigint(20) unsigned NOT NULL COMMENT '商户ID',
  `mch_name` varchar(50) NOT NULL COMMENT '商户名称',
  `mch_type` varchar(24) NOT NULL COMMENT '商户类型',
  `pri_key` varchar(1024) NOT NULL COMMENT '平台私钥',
  `pub_key` varchar(256) NOT NULL COMMENT '平台公钥',
  `mch_pub_key` varchar(256) NOT NULL COMMENT '商户公钥',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态 0:正常, 1:禁用, -1:删除',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `create_by` int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `update_by` int(11) NOT NULL DEFAULT '0' COMMENT '修改人id',
  PRIMARY KEY (`mch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商户信息表';

-- ----------------------------
-- 9、APP应用表
-- ----------------------------
DROP TABLE IF EXISTS `pay_app`;
CREATE TABLE `pay_app` (
  `app_id` varchar(32) NOT NULL COMMENT 'APPID',
  `mch_id` bigint(20) unsigned NOT NULL COMMENT '商户ID',
  `app_name` varchar(50) NOT NULL COMMENT 'APP名称',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态 0:正常, 1:禁用, -1:删除',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `create_by` int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `update_by` int(11) NOT NULL DEFAULT '0' COMMENT '修改人id',
  PRIMARY KEY (`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='APP信息表';

-- ----------------------------
-- 10、渠道表
-- ----------------------------
DROP TABLE IF EXISTS `pay_channel`;
CREATE TABLE `pay_channel` (
  `channel_id` varchar(32) NOT NULL COMMENT '渠道ID',
  `channel_name` varchar(30) NOT NULL COMMENT '渠道名称',
  `channel_desc` varchar(50) DEFAULT '' COMMENT '渠道描述',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态 0:正常, 1:禁用, -1:删除',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `create_by` int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `update_by` int(11) NOT NULL DEFAULT '0' COMMENT '修改人id',
  PRIMARY KEY (`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='渠道表';

-- ----------------------------
-- 11、支付流水表
-- ----------------------------
DROP TABLE IF EXISTS `pay_trade`;
CREATE TABLE `pay_trade` (
  `trade_id` bigint(20) unsigned NOT NULL COMMENT '本次交易唯一id，整个支付系统唯一，生成他的原因主要是 order_id对于其它应用来说可能重复',
  `app_order_id` varchar(64) NOT NULL COMMENT '应用方订单号',
  `app_id` varchar(32) NOT NULL COMMENT '应用id',
  `channel_id` varchar(32) NOT NULL COMMENT '支付渠道ID(如：alipay、wechatpay、unionpay)',
  `pay_type` varchar(32) NOT NULL COMMENT '支付类型，比如：微信支付[JSAPI、NATIVE、APP、MWEB]',
  `total_fee` int(9) NOT NULL DEFAULT '0' COMMENT '支付金额，整数方式保存',
  `scale` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '金额对应的小数位数',
  `currency_code` char(3) NOT NULL DEFAULT 'CNY' COMMENT '交易的币种',
  `channel_user_id` varchar(32) DEFAULT NULL COMMENT '渠道用户标识,如微信openId,支付宝账号',
  `extra` varchar(512) DEFAULT NULL COMMENT '特定渠道发起时额外参数',
  `notify_url` varchar(255) NOT NULL COMMENT '支付后，异步通知url',
  `return_url` varchar(255) DEFAULT NULL COMMENT '支付后跳转url',
  `body` varchar(255) NOT NULL DEFAULT '' COMMENT '商品描述信息',
  `client_ip` varchar(12) NOT NULL DEFAULT '' COMMENT '客户端ip',
  `expire_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '订单过期时间',
  `payment_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '第三方支付成功的时间',
  `finish_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '通知上游系统的时间',
  `trade_no` varchar(64) DEFAULT NULL COMMENT '第三方的流水号(异步通知的时候更新)',
  `order_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:等待支付，1:完成支付， 2:业务处理完成，3:该笔交易已关闭，-1:支付失败',
  `erorr_code` varchar(64) DEFAULT NULL COMMENT '渠道支付错误码',
  `erorr_msg` varchar(128) DEFAULT NULL COMMENT '渠道支付错误描述',
  `param1` varchar(64) DEFAULT NULL COMMENT '扩展参数1',
  `param2` varchar(64) DEFAULT NULL COMMENT '扩展参数2',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`trade_id`),
  KEY `idx_trade_no` (`trade_no`),
  KEY `idx_create_time` (`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='支付流水表';

-- ----------------------------
-- 12、退款流水表
-- ----------------------------
DROP TABLE IF EXISTS `pay_refund`;
CREATE TABLE `pay_refund` (
  `refund_id` bigint(20) unsigned NOT NULL COMMENT '退款id',
  `trade_id` bigint(20) unsigned NOT NULL COMMENT '交易id',
  `app_refund_id` varchar(64) NOT NULL COMMENT '应用方退款id',
  `trade_no` varchar(64) NOT NULL COMMENT '第三方的支付流水号',
  `app_id` varchar(32) NOT NULL COMMENT '应用id',
  `channel_id` varchar(32) NOT NULL COMMENT '支付渠道ID(如：alipay、wechatpay、unionpay)',
  `pay_type` varchar(32) NOT NULL COMMENT '支付类型，比如：微信支付[JSAPI、NATIVE、APP、MWEB]',
  `refund_fee` int(9) NOT NULL DEFAULT '0' COMMENT '退款金额，整数方式保存',
  `scale` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '金额对应的小数位数',
  `currency_code` char(3) NOT NULL DEFAULT 'CNY' COMMENT '交易的币种(CNY、USD、HKD)',
  `channel_user_id` varchar(32) DEFAULT NULL COMMENT '渠道用户标识,如微信openId,支付宝账号',
  `extra` varchar(512) DEFAULT NULL COMMENT '特定渠道发起时额外参数',
  `notify_url` varchar(255) NOT NULL COMMENT '退款后，异步通知url',
  `refund_no` varchar(64) NOT NULL DEFAULT '' COMMENT '第三方退款流水号',
  `expire_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '订单过期时间',
  `refund_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '第三方退款成功的时间',
  `finish_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '通知上游系统的时间',
  `refund_reason` varchar(128) DEFAULT NULL COMMENT '退款理由',
  `client_ip` varchar(12) NOT NULL DEFAULT '' COMMENT '客户端ip',
  `refund_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '退款类型；0:业务退款; 1:重复退款',
  `refund_way` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '退款方式：1自动原路返回; 2人工打款',
  `refund_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0待退款; 1退款成功; 2业务处理完成; 3该笔交易已关闭; -1退款不成功',
  `erorr_code` varchar(64) DEFAULT NULL COMMENT '渠道支付错误码',
  `erorr_msg` varchar(128) DEFAULT NULL COMMENT '渠道支付错误描述',
  `param1` varchar(64) DEFAULT NULL COMMENT '扩展参数1',
  `param2` varchar(64) DEFAULT NULL COMMENT '扩展参数2',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`refund_id`),
  KEY `idx_trade_no` (`trade_no`),
  KEY `idx_status` (`refund_status`),
  KEY `idx_create_time` (`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='退款流水表';

-- ----------------------------
-- 13、上游通知表
-- ----------------------------
DROP TABLE IF EXISTS `pay_notify`;
CREATE TABLE `pay_notify` (
  `notify_id` bigint(20) unsigned NOT NULL COMMENT '通知ID',
  `app_id` varchar(32) NOT NULL COMMENT '应用id',
  `trade_id` bigint(20) unsigned NOT NULL COMMENT '交易id',
  `app_order_id` varchar(64) NOT NULL COMMENT '应用方订单号',
  `notify_type` varchar(10) NOT NULL COMMENT '通知类型:pay、refund、canceled',
  `notify_url` varchar(255) NOT NULL COMMENT '通知地址',
  `notify_count` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '通知次数',
  `notify_status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '通知状态,1-通知中,2-通知成功,3-通知失败',
  `last_notify_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '最后一次通知时间',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`notify_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='上游通知表';

-- ----------------------------
-- 14、交易日志表
-- ----------------------------
DROP TABLE IF EXISTS `pay_log`;
CREATE TABLE `pay_log` (
  `log_id` bigint(20) unsigned NOT NULL COMMENT '日志ID',
  `app_id` varchar(32) NOT NULL COMMENT '应用id',
  `app_order_id` varchar(64) NOT NULL COMMENT '应用方订单号',
  `log_type` varchar(10) NOT NULL COMMENT '日志类型，payment:支付; refund:退款; notify:异步通知; query:查询',
  `request_header` text COMMENT '请求的header 头',
  `request_params` text COMMENT '支付的请求参数',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `client_ip` varchar(12) NOT NULL DEFAULT '' COMMENT '客户端ip',
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='交易日志表';

-- ----------------------------
-- 15、支付宝支付配置表
-- ----------------------------
DROP TABLE IF EXISTS `pay_ali_config`;
CREATE TABLE `pay_ali_config` (
  `id` bigint(20) unsigned NOT NULL COMMENT 'ID',
  `pid` varchar(30) NOT NULL COMMENT '合作伙伴身份ID',
  `seller_id` varchar(50) NOT NULL COMMENT '支付宝企业账户(邮箱)',
  `alipay_app_id` varchar(30) NOT NULL COMMENT '支付宝应用ID',
  `md5_secret` varchar(32) NOT NULL COMMENT 'MD5秘钥',
  `sign_type` varchar(8) NOT NULL COMMENT '加密方式',
  `public_key` varchar(512) NOT NULL COMMENT '支付宝公钥',
  `private_key` varchar(2048) NOT NULL COMMENT '支付宝应用私钥',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态 0:正常, 1:禁用, -1:删除',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `create_by` int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `update_by` int(11) NOT NULL DEFAULT '0' COMMENT '修改人id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='支付宝支付配置表';

-- ----------------------------
-- 16、支付宝支付配置和APP关联表
-- ----------------------------
DROP TABLE IF EXISTS `pay_ali_app`;
CREATE TABLE `pay_ali_app` (
  `config_id` int(11) NOT NULL COMMENT '支付宝支付配置ID(对应pay_ali_config表id)',
  `app_id` varchar(30) NOT NULL COMMENT 'APPID',
  PRIMARY KEY (`config_id`,`app_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='支付宝支付配置和APP关联表';

-- ----------------------------
-- 17、微信支付配置表
-- ----------------------------
DROP TABLE IF EXISTS `pay_wechat_config`;
CREATE TABLE `pay_wechat_config` (
  `id` bigint(20) unsigned NOT NULL COMMENT 'ID',
  `wechatpay_app_id` varchar(30) NOT NULL COMMENT '微信应用ID',
  `app_secret` varchar(64) DEFAULT NULL COMMENT '应用秘钥',
  `merchant_id` varchar(32) NOT NULL COMMENT '商户ID',
  `api_secret` varchar(32) NOT NULL COMMENT 'API秘钥',
  `employee_id` varchar(64) DEFAULT NULL COMMENT '员工账号(用于退款及退款查询)',
  `api_certificate` varchar(128) NOT NULL COMMENT 'API证书',
  `private_key` varchar(2048) NOT NULL COMMENT 'API证书秘钥',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态 0:正常, 1:禁用, -1:删除',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `create_by` int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `update_by` int(11) NOT NULL DEFAULT '0' COMMENT '修改人id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='微信支付配置表';

-- ----------------------------
-- 18、微信支付配置和APP关联表
-- ----------------------------
DROP TABLE IF EXISTS `pay_wechat_app`;
CREATE TABLE `pay_wechat_app` (
  `config_id` int(11) NOT NULL COMMENT '微信支付配置ID(对应pay_wechat_config表id)',
  `app_id` varchar(30) NOT NULL COMMENT 'APPID',
  PRIMARY KEY (`config_id`,`app_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='微信支付配置和APP关联表';
