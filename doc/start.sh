#!/bin/bash
LANG="zh_CN.UTF-8"

APP_HOME=$(echo `pwd` | sed 's/bin//')
APPPIDFILE=$APP_HOME/app.pid
source /etc/profile

case $1 in
start)
    echo  "Starting server... "

    HEAP_MEMORY=256m
    PERM_MEMORY=64m
    JMX_PORT=2632
    JMX_HOST=106.75.74.53
    JAVA_OPTS="-server -XX:+UseConcMarkSweepGC -XX:+PrintGCDetails -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=jvm.log -Djava.net.preferIPv4Stack=true"

    shift
    ARGS=($*)
    for ((i=0; i<${#ARGS[@]}; i++)); do
        case "${ARGS[$i]}" in
        -D*)    JAVA_OPTS="${JAVA_OPTS} ${ARGS[$i]}" ;;
        -Heap*) HEAP_MEMORY="${ARGS[$i+1]}" ;;
        -Perm*) PERM_MEMORY="${ARGS[$i+1]}" ;;
        -JmxPort*)  JMX_PORT="${ARGS[$i+1]}" ;;
        -JmxHost*)  JMX_HOST = "${ARGS[$i+1]}" ;;
        esac
    done
    JAVA_OPTS="${JAVA_OPTS} -Xms${HEAP_MEMORY} -Xmx${HEAP_MEMORY} -XX:PermSize=${PERM_MEMORY} -XX:MaxPermSize=${PERM_MEMORY} -XX:MaxDirectMemorySize=128m"
    echo "start jvm args ${JAVA_OPTS}"

    nohup java $JAVA_OPTS -cp .:./tem-pay-1.0.jar org.springframework.boot.loader.JarLauncher &
    echo $! > $APPPIDFILE
    echo STARTED
    ;;

stop)
    echo "Stopping server ... "
    if [ ! -f $APPPIDFILE ]
    then
        echo "error: count not find file $APPPIDFILE"
        exit 1
    else
        kill -9 $(cat $APPPIDFILE)
        rm $APPPIDFILE
        echo STOPPED
    fi
    ;;

*)
    echo "Please enter start|stop ... "
    ;;

esac

exit 0
